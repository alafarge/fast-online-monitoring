from utils import *
PATH_FIG = "/mnt/c/Users/Arthur/Documents/Code/Work/HGTD/TestBeam/Fig"

def lsbs_plot(lsbs, campaign, saveFig=None):
    fig, ax = plt.subplots(figsize=(8,6))

    y = np.array([lsbs[i,:] for i in range(225)])
    x = np.array([[i] for i in range(225)])

    rm0 = np.any(y != 0, axis=1)
    x= x[rm0]
    y=y[rm0]

    if y.shape[1] == 4:
        labels = ['0 - 32', '32 - 64', '64 - 96', '96 - 128']
    elif y.shape[1] == 1:
        labels = ['0 - 128']
    markers = ['o', 's', 'v', '^', 'D', 'P', '*']
    colors = ['dimgrey', 'indianred', 'steelblue', 'violet', 'darkorange', 'darkgreen', 'darkcyan']
    # Plotting

    for i in range(lsbs.shape[1]):
        if y[:,i].shape[0] == 0:
            continue
        elif y[:,i].shape[0] == 1:
            val, val_u = y[:,i][0], 0
        else:
            val, val_u,_ = fit_constant(x, y[:,i])
        ax.hlines(val, np.min(x), np.max(x), color=colors[i], lw=1, linestyle='--')
        ax.fill_between(x[:,0], val-val_u, val+val_u, color=colors[i], alpha=0.2)
        ax.plot(x, y[:,i], lw=0,  color=colors[i] , markersize=5, label=labels[i]+r" $LSB_{fit}=$"+f"{val:.1f}"+r"$\pm$"+f"{val_u:.1f}", marker=markers[i])

    ax.set_xlabel("Pixel", fontweight='bold', fontsize=13)
    ax.set_ylabel('LSB TOA [ps]', fontweight='bold', fontsize=13)
    ax.legend(ncol=2)
    ax.text(0.03, 0.97, 'HGTD Test Beam', transform=ax.transAxes,fontsize=14, va='top', fontweight='bold', fontstyle='italic')
    ax.text(0.0, 1.03, f'{campaign}', horizontalalignment='left', verticalalignment='top', transform=ax.transAxes, size=9)
    ax.grid(True, alpha=0.3)
    plt.tight_layout()
    
    if type(saveFig) == str:
        plt.savefig(saveFig)

def resolutions_plot(resolutions, resolutions_u, campaign, saveFig=None):
    fig, ax = plt.subplots(figsize=(8,6))

    x = np.array([[i] for i in range(225)])
    y = resolutions
    y_u = resolutions_u

    rm0 = y>0
    x= x[rm0]
    y=y[rm0]
    y_u=y_u[rm0]
    print(y_u)

    val, val_u,chi2 = fit_constant(x, y, y_u)
    chi2_text = f"$\chi^2$/NDF: {chi2:.2f}/{len(x)-1}"
    ax.hlines(val, np.min(x), np.max(x), color='dimgrey', lw=1, linestyle='--')
    ax.errorbar(x, y, yerr=y_u, lw=0,elinewidth=1,  color='dimgrey' , markersize=5,  marker='o',
                label=r"$\sigma_{fit}=$"+f"{val:.1f}"+r"$\pm$"+f"{val_u:.1f}"+f"\n{chi2_text}",)
    ax.fill_between(x[:,0], val-val_u, val+val_u, color='dimgrey', alpha=0.2)
    ax.set_xlabel("Pixel", fontweight='bold', fontsize=13)
    ax.set_ylabel('Resolution [ps]', fontweight='bold', fontsize=13)
    ax.legend(ncol=2)
    ax.text(0.03, 0.97, 'HGTD Test Beam', transform=ax.transAxes,fontsize=14, va='top', fontweight='bold', fontstyle='italic')
    ax.text(0.0, 1.03, f'{campaign}', horizontalalignment='left', verticalalignment='top', transform=ax.transAxes, size=9)
    ax.grid(True, alpha=0.3)
    plt.tight_layout()
    
    if type(saveFig) == str:
        plt.savefig(saveFig)
        
    return val, val_u

def Occupancy(x,y, saveFig=None):
    bin_x=np.arange(0, 16, 1)

    fig = plt.figure(figsize=(10, 10))

    gs = fig.add_gridspec(2, 2,  width_ratios=(4, 1), height_ratios=(1, 4),
                      left=0.1, right=0.9, bottom=0.1, top=0.9,
                      wspace=0.04, hspace=0.12)
    ax = fig.add_subplot(gs[1, 0])
    ax.set_xlabel('Column', weight='bold', fontsize=14)

    ax.set_ylabel('Row', weight='bold', fontsize=14)
    ax_histx = fig.add_subplot(gs[0, 0], sharex=ax)
    ax_histy = fig.add_subplot(gs[1, 1], sharey=ax)
    hx=ax_histx.hist(x, bins=15, range=(0,15), color='darkslategray')
    hy=ax_histy.hist(y, bins=15, orientation='horizontal', range=(0,15),  color='darkslategray')
    ax_histx.tick_params(axis="x", labelbottom=False)
    ax_histy.tick_params(axis="y", labelleft=False)
    ax_histx.text(np.mean(x),np.max(hx[0])/3,'Mean = {:.2f}'.format(np.mean(x)))
    ax_histx.text(np.mean(x),np.max(hx[0])/4,'Std = {:.2f}'.format(np.std(x)))
    ax_histy.text(np.max(hy[0])/3,np.mean(y),'Mean = {:.2f}'.format(np.mean(y)), rotation=-90)
    ax_histy.text(np.max(hy[0])/4,np.mean(y),'Std = {:.2f}'.format(np.std(y)), rotation=-90)   
     
    h2d = ax.hist2d( x,y, bins=[bin_x, bin_x] ,cmap = 'viridis_r', cmin = 0.5)
    M=h2d[0]
    M[np.isnan(M)]=0
    counts = np.nan_to_num(h2d[0])
    ax.set_title(f'#Hits : {int(np.nansum(h2d[0]))}')
    ax.text(0.01, 1.03, 'HGTD Test Beam', transform=ax.transAxes,fontsize=14, va='top', fontweight='bold', fontstyle='italic')

    # fig.tight_layout()

    # counting/pixel
    for i in range(counts.shape[0]):
        for j in range(counts.shape[1]):
            c = counts[i,j]
            ax.text(bin_x[i]+0.5, bin_x[j]+0.5, int(c), fontsize=6,
                        ha = 'center', va = 'center', color = 'black', weight='bold')
    
    if saveFig!=None:
        fig.savefig(f"{saveFig}/Occupancy.png", bbox_inches='tight')
    return M

def signalPlot(comment=''):
    
    from matplotlib.ticker import AutoMinorLocator
    fig, ax = plt.subplots(1, 3, figsize=(14,4))
    plt.tight_layout()

    for axis in ax:
        axis.text(0.03, 0.97, 'HGTD '+r"$\rm{\it{TestBeam}}}$", transform=axis.transAxes,fontsize=12, va='top', fontweight='bold', fontstyle='italic')
        axis.text(0.0, 1.05, comment, horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, size=9)
        axis.grid(True, alpha=0.3)
        axis.tick_params(right=True, top=True, direction='in')
        axis.tick_params(which='major', length=5, right=True, top=True, direction='in')
        axis.tick_params(which='minor', length=2, right=True, top=True, direction='in')
        axis.xaxis.set_minor_locator(AutoMinorLocator(3))
        axis.yaxis.set_minor_locator(AutoMinorLocator(3))   
        
    return fig, ax

def myPlot(xlabel='X', ylabel='Y', comment='', doRatio=False):
    from matplotlib.ticker import AutoMinorLocator
    if doRatio:
        fig, ax = plt.subplots(2, 1, figsize=(7,6), gridspec_kw={'height_ratios': [3, 1], 'hspace':0.05},sharex=True)
        ax[1].set_xlabel(xlabel, fontweight='bold', fontsize=13)
    else:
        fig, ax = plt.subplots(figsize=(7,6))
        ax = tuple([ax])
        ax[0].set_xlabel(xlabel, fontweight='bold', fontsize=13)

    ax[0].set_ylabel(ylabel, fontweight='bold', fontsize=13)
    ax[0].text(0.03, 0.97, 'HGTD '+r"$\rm{\it{Test Beam}}$", transform=ax[0].transAxes,fontsize=14, va='top', fontweight='bold', fontstyle='italic')
    ax[0].text(0.0, 1.03, comment, horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, size=9)
    ax[0].grid(True, alpha=0.3)

    for axis in ax:
        axis.tick_params(right=True, top=True, direction='in')
        axis.tick_params(which='major', length=5, right=True, top=True, direction='in')
        axis.tick_params(which='minor', length=2, right=True, top=True, direction='in')
        axis.xaxis.set_minor_locator(AutoMinorLocator(3))
        axis.yaxis.set_minor_locator(AutoMinorLocator(3))   

    return fig, ax
        