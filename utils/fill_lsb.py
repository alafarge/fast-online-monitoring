import ROOT
import numpy as np
import os, sys, re
curpath = os.getcwd()
sys.path.insert(0,curpath)
from array import array
import argparse
import utils as u
import uproot as up
    
parser = argparse.ArgumentParser(description='Compute LSB from deltaT')
parser.add_argument('merged_file', metavar='merged_file', type=str, help='Merged file name .root')

def ExtractValuesOfBranch(fileName, treeName, branchName, varType="double", tcut=""):

    ROOT.gInterpreter.Declare("""
    #include <TFile.h>
    #include <TTree.h>

    std::vector<double> extract_branch_values_d(TFile* file, const char* treeName, const char* branchName, const char* tCut="") {
        TTree *tree = (TTree*)file->Get(treeName);
        std::vector<double> values;
        double value;
        TBranch *branch = tree->GetBranch(branchName);
        branch->SetAddress(&value);
        
        // Compile the TCut expression if provided
        TTreeFormula* cut = nullptr; 
        if(strlen(tCut) > 0){
            cut = new TTreeFormula("cut", tCut, tree);
        }
  
        for (int i=0; i<tree->GetEntries(); ++i) {
            branch->GetEntry(i);
            if(cut != nullptr && !cut->EvalInstance()) continue; // If a cut is defined, apply it on the fly
            if (value != -999) {
                values.push_back(value);
                }
        }

        if(cut != nullptr) delete cut; // cleanup
        return values;
    }

    std::vector<int> extract_branch_values_i(TFile* file, const char* treeName, const char* branchName) {
        TTree *tree = (TTree*)file->Get(treeName);
        std::vector<int> values;
        int value;
        TBranch *branch = tree->GetBranch(branchName);
        branch->SetAddress(&value);
        
        for (int i=0; i<tree->GetEntries(); ++i) {
            branch->GetEntry(i);
            if (value != -999) {
                values.push_back(value);
                }
        }

        return values;
    }
    """)
    inFile = ROOT.TFile(fileName)
    
    if varType == "double":
        values = ROOT.std.vector('double')(ROOT.extract_branch_values_d(inFile, treeName, branchName, tCut=tcut))
    elif varType == "int":
        values = ROOT.std.vector('int')(ROOT.extract_branch_values_i(inFile, treeName, branchName))        
    
    return np.array(values)

def lsb_to_fill(path_data, lsb, toaSegments=[0,32,64,96, 128]):
    tree = up.open(path_data)['Sequoia']
    col = tree['nCol'].array(library='np')
    row = tree['nRow'].array(library='np')
    toa = -tree['tTOA'].array(library='np')

    pixels = u.to_pixel(col, row)
    lsb_ = np.zeros(len(toa))

    for pix in pixels:
        sel_p = pixels==pix

        for i in range(len(toaSegments)-1):
            sel_toa = (toa>=toaSegments[i]) & (toa<toaSegments[i+1])
            lsb_[sel_toa&sel_p] = lsb[pix,i]

    return lsb_

def add_lsb(file_name, lsb):
    root_file = ROOT.TFile(file_name,'update')
    tree = root_file.Get("Sequoia")

    # add lsb value to  a tree
    lsbTOA = array('d', [0])
    lsb_branch = tree.Branch('lsbUpdate', lsbTOA, 'lsbUpdate/D')
    
    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        pixel=u.to_pixel([tree.nCol], [tree.nRow])
        lsbTOA[0] = lsb[pixel[0]]
        lsb_branch.BackFill()
        
    tree.Write("",ROOT.TObject.kOverwrite)
    root_file.Close()
    print("Finished adding LSB to the tree")

def estimate_null_elements(lsb):
    non_zero = np.all(lsb!=0, axis=1)
    estimates = np.mean(lsb[non_zero], axis=0)
    lsb[~non_zero] = np.tile(estimates, (lsb[~non_zero].shape[0],1))

    return lsb
            

def update_lsb(file_name, lsb, toaSegments=[0,32,64,96, 128]):
    lsb = estimate_null_elements(lsb)
    lsb_ = lsb_to_fill(file_name, lsb, toaSegments=toaSegments)
    add_lsb(file_name, lsb_)

    

    
    
    