
# Fit settings
totResolution = 160
window_fit=(-300,300)

#TOT window range in 160ps unit around the TOT peak -> Essentially to avoid weird TW correction 
window_tot = (6, 12)
window_tot_tw = (3, 4)

# Paths 
PATH_ATLAS_STYLE = '/afs/cern.ch/user/h/hgtdtb/Software/Scripts/OnlineMonitoring/utils/AtlasStyle.C'
PATH_FIG = "/eos/user/h/hgtdtb/www/Campaigns/{}/figures/figures-{}"
DISK= "/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam"

# Required events
# --->  To be changes in AnalysisSkew.C too -> required_stat
stats_batch = 800
stats_run = 110

# CUTS
tdc_clock_cut = -100.
# for Nov23 was 45
probe_amblitude_cut = 35.