import matplotlib.pyplot as plt
import ROOT
import uproot as up
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
from scipy.optimize import curve_fit
import json
import settings as s


def to_pixel(col, row):
    if type(col) != type(np.zeros(1)):
        col = np.array(col)
        row = np.array(row)
    pixels = col*15+row
    return pixels

def to_col_row(pixels):
    if type(pixels) != type(np.zeros(1)):
        pixels = np.array(pixels)
    col = pixels//15
    row = pixels%15
    
    return col, row

def import_style():
    ROOT.gROOT.LoadMacro(s.PATH_ATLAS_STYLE)
    ROOT.SetAtlasStyle()  
    ROOT.gStyle.SetOptFit(111);
    ROOT.gStyle.SetPaintTextFormat("2.1f");

def set_label(name = ""):
    l=ROOT.TLatex();
    l.SetTextSize(0.04); 
    l.SetNDC();
    l.SetTextColor(ROOT.kBlack);
    l.SetTextFont(72);
    l.DrawLatex(0.17,0.96,f"HGTD Test Beam {name}");
    
    return l

def set_info(x=0.2, y=0.88, info=""):
    l=ROOT.TLatex();
    l.SetNDC();
    l.SetTextFont(43)
    l.SetTextSize(20)
    l.SetTextColor(ROOT.kBlack);
    l.DrawLatex(x,y,info);
    return l

def get_binning(data, resolution=80):
    xMin = np.min(data)
    xMax = np.max(data)
    xRange = (xMin,xMax)
    xBins = int((xMax-xMin)/resolution)
    return xBins, xRange

def center_histogram(data, resolution=80, sel=None, plot=False):
    xBins, xRange = get_binning(data, resolution=resolution)
    hist, bin_edges = np.histogram(data, bins=xBins, range=xRange)

    shift_amount = bin_edges[np.argmax(hist)]
    centered_bins = bin_edges - shift_amount
    
    if plot:
        plt.bar(centered_bins[:-1], hist, width=np.diff(centered_bins), edgecolor='black')
        plt.xlabel('Value')
        plt.ylabel('Frequency')

    return shift_amount
    
def remove_infrequent_elements(arr, min_occurrences=4):
    # Count occurrences of each element in the array
    counts = Counter(arr)

    # Identify elements to keep (those with at least min_occurrences occurrences)
    elements_to_keep = {key for key, value in counts.items() if value >= min_occurrences}

    # Create a boolean mask to select elements to remove
    remove_mask = np.array([x not in elements_to_keep for x in arr])

    # Use NumPy to filter the array
    filtered_arr = arr[~remove_mask]

    return filtered_arr, remove_mask

def gaussian_fit(data, binning, fitRange=None):
    if fitRange is None:
        fitRange = (binning[1], binning[2])
    
    hist = ROOT.TH1F("", "", *binning)
    
    [hist.Fill(value) for value in data]
        
    fit_func = ROOT.TF1("fit_func", "gaus", fitRange[0], fitRange[1])
    fit_func.SetParameters(np.max(hist), np.mean(data), np.std(data))
    hist.Fit(fit_func, "QR")
    mean = fit_func.GetParameter(1)
    mean_u = fit_func.GetParError(1)
    rms = fit_func.GetParameter(2)
    rms_u = fit_func.GetParError(2)
    
    return hist, fit_func, [mean, mean_u, rms, rms_u]

def fit_gaussian(data, binning, saveFig=None, infoFig=''):

    hist, fit_func,[mean0,mean_u, rms, rms_u] = gaussian_fit(data, binning, fitRange=None)    
    data=data-mean0
    hist, fit_func,[mean,mean_u, rms, rms_u]= gaussian_fit(data, binning, fitRange=(-1.7*rms,2.*rms) )  
    # Display the histogram and fit if specified
    if type(saveFig) == str:
        import_style() 
        canvas = ROOT.TCanvas("canvas", "Fit Canvas", 800, 600)
        hist.Draw()
        fit_func.Draw("same")
        hist.GetXaxis().SetTitle("#bf{t_{asic}-(t_{mcp}-t_{clk}) [ps]}")
        hist.GetYaxis().SetTitle("#bf{Events}")
        set_info(info=infoFig)
        set_label(name = "")
        canvas.Update()
        canvas.Modified()
        canvas.Print(saveFig)

    return data,mean0+mean, [rms,rms_u]

def best_resolution(toa, mcp, clock, resolutions_, lsb_, saveFig=None):
    rms_ = np.zeros((len(resolutions_), len(lsb_)))
    rms_u_ = np.zeros((len(resolutions_), len(lsb_)))
    best_res = np.inf

    for ith, i in enumerate(resolutions_):
        binning= (int((s.window_fit[1]-s.window_fit[0])/i), s.window_fit[0],s.window_fit[1] )
        for jth, j in enumerate(lsb_):
            dt =(-toa*j - (mcp - clock))
            center = center_histogram(dt)
            dt=dt-center
            selPeak = (dt>s.window_fit[0]) & (dt<s.window_fit[1])
            _, mean,[rms, rms_u]= fit_gaussian(dt[selPeak], binning, saveFig=False)
            rms_[ith, jth] = rms
            rms_u_[ith, jth] = rms_u
            if rms_u+rms < best_res:
                best_res = rms+rms_u
                best_data = dt
                params=[i,j, rms,rms_u, center+mean]
                
    binning= (int((s.window_fit[1]-s.window_fit[0])/params[0]), s.window_fit[0],s.window_fit[1] )
    # fit_gaussian(best_data, binning, saveFig=saveFig, infoFig=f"Pixel - {PIX}")        
    return params, best_data

# Iteration of bin size and lsb to define the best LSB
def find_best_resolution(toa, mcp, clock, provideLSB=False):
    #1st iteration
    resolutions_ = np.arange(8,50,2)
    if type(provideLSB)==float:
        lsb_ = [provideLSB]
    else :
        lsb_ = np.linspace(13,25, 14)
    params, _ = best_resolution(toa, mcp, clock,resolutions_, lsb_)
    # 2nd iteration
    resolutions_ = np.arange(params[0]-1,params[0]+1,1)
    lsb_ = np.linspace(params[1]-1,params[1]+1, 10)
    params, _ = best_resolution(toa, mcp, clock,resolutions_, lsb_)
    return params


def lsb_interval(toa, mcp, clock, interval=[0,32,64,96, 128], provideLSB=False):
    res = np.zeros((len(interval)-1,6))
    for i in range(len(interval)-1):
        if type(provideLSB)==list:
            lsb = provideLSB[i]
        else:
            lsb=False
        sel_toa = (toa>=interval[i])&(toa<interval[i+1])
        params = find_best_resolution(toa[sel_toa], mcp[sel_toa], clock[sel_toa], provideLSB=lsb)
        
        # print(f"LSB: {params[1]:.2f} \t Resolution: {params[2]:.2f}+/-{params[3]:.2f}")
        
        res[i,:] = [interval[i+1], *params]
        
    return res

def get_entries(PATH_DATA):
    tree = up.open(PATH_DATA)['Sequoia']
    return len(tree['tTOA'].array(library='np'))


def correct_toa(toa, lsb, interval, centers):
    shifts = np.zeros(len(toa))
    toa_corrected = np.zeros(len(toa))
    for i in range(len(interval)-1):
        sel_toa = (toa>=interval[i])&(toa<interval[i+1])
        toa_corrected[sel_toa] = toa[sel_toa]*lsb[i]
        shifts[sel_toa] = shifts[sel_toa]-centers[i]

    return toa_corrected, shifts

def correct_time_walk(dt, tot, binning_tot, binning_dt, order=3, pixel=0, saveFig=None):   
    import_style() 
    # ROOT.gStyle.SetOptFit(111);
    # ROOT.gStyle.SetPaintTextFormat("2.1f");
    canvas = ROOT.TCanvas("canvas", "", 800, 800)
    
    # Initiaslisation of histograms
    profile = ROOT.TProfile("", "", int(binning_tot[2]), binning_tot[0],binning_tot[1], binning_dt[0], binning_dt[1])
    hist2d = ROOT.TH2D("", "", int(binning_tot[2]), binning_tot[0], binning_tot[1], int(binning_dt[2]), binning_dt[0], binning_dt[1])
    
    # Fill histograms
    for i in range(len(dt)):
        hist2d.Fill(tot[i], dt[i])
        profile.Fill(tot[i], dt[i])
        
    # Do the fit
    fit_func = ROOT.TF1("fit_func", f"pol{order}", binning_tot[0], binning_tot[1])
    profile.Fit(fit_func,"RQ")
    fit_results = profile.GetFunction("fit_func")
    params_tw = [fit_results.GetParameter(i) for i in range(order+1)]

    if fit_results.GetNDF()==0:
        return None
    
    if type(saveFig) == str:    
        # Draw
        hist2d.Draw("COLZ")
        profile.Draw("same")
        fit_func.Draw("same")
        # #set marker
        profile.SetMarkerStyle(20)
        profile.SetMarkerSize(1)
        
        # #axis labels
        hist2d.GetXaxis().SetTitle("#bf{TOT [ps]}")
        hist2d.GetYaxis().SetTitle("#bf{#Deltat [ps]}")

        # # Add legend
        canvas.SetLeftMargin(0.15)
        canvas.SetRightMargin(0.15)
        
        canvas.Update()
        canvas.Draw()

        #display fit parameters
        text = ROOT.TLatex()
        text.SetNDC()
        text.SetTextFont(43)
        text.SetTextSize(20)
        text.DrawLatex(0.2, 0.9, "#chi^{2}/NDF:"+" {:.2f}/{}".format(fit_results.GetChisquare(),fit_results.GetNDF())+f" - Pixel: {pixel}")
        l=set_label(name = "")
        
        # Save fig 
        canvas.SaveAs(saveFig)

    return params_tw

def linearity(toa, tot, mcp, clock, pixels, saveFig=None, stats=150):   
    import_style() 
    # ROOT.gStyle.SetOptFit(111);
    # ROOT.gStyle.SetPaintTextFormat("2.1f");
    store_lsb = np.zeros(225)
    for pix in np.unique(pixels):
        sel_p = (pixels == pix) & (tot!=0)
                
        if np.count_nonzero(sel_p)<stats:
            continue

        toa_p = toa[sel_p]
        tot_p = tot[sel_p]
        mcp_p = mcp[sel_p]
        clock_p = clock[sel_p]
        dt = clock_p-mcp_p

        if len(toa_p)<stats:
            continue

        totLimit_l, totLimit_h = tot_limits(tot_p, interval=(s.window_tot[0],s.window_tot[1]))
        cut = (mcp_p!=0) & ((-mcp_p+clock_p)<0) & (tot_p>=totLimit_l) & (tot_p<totLimit_h)

        canvas = ROOT.TCanvas("canvas", "", 800, 800)
        dtLimit_l, dtLimit_h = tot_limits(dt[cut], interval=(11,11))

        # Initiaslisation of histograms
        profile = ROOT.TProfile("", "", 128,-0.5 ,127.5, dtLimit_l, dtLimit_h)
        hist2d = ROOT.TH2D("", "", 128,-0.5, 127.5, int((dtLimit_h-dtLimit_l)/64), dtLimit_l, dtLimit_h)
        
        # Fill histograms
        for i in range(len(dt[cut])):
            hist2d.Fill((toa_p[cut])[i], (dt[cut])[i])
            profile.Fill((toa_p[cut])[i], (dt[cut])[i])
            
        # Do the fit
        fit_func = ROOT.TF1("fit_func", "pol1")
        profile.Fit(fit_func,"Q")
        fit_results = profile.GetFunction("fit_func")

        bin_filled = []
        minMaxContent = []
        for i in range(1, profile.GetNbinsX() + 1):
            if profile.GetBinEntries(i) >0:
                bin_filled.append(i)
                minMaxContent.append(profile.GetBinContent(i))
        
        yLow = fit_results.GetParameter(0) + fit_results.GetParameter(1) * (-1)
        yHigh = fit_results.GetParameter(0) + fit_results.GetParameter(1)* 128
        hist2d.GetYaxis().SetRangeUser(min(minMaxContent),max(minMaxContent))
        store_lsb[pix] = fit_results.GetParameter(1)

        if type(saveFig) == str:    
            # Draw
            hist2d.Draw("COLZ")
            profile.Draw("same")
            fit_func.Draw("same")
            # #set marker
            profile.SetMarkerStyle(20)
            profile.SetMarkerSize(1)
            
            # #axis labels
            hist2d.GetXaxis().SetTitle("#bf{TOA [DAC]}")
            hist2d.GetYaxis().SetTitle("#bf{t_{clock}-t_{MCP} [ps]}")

            # # Add legend
            canvas.SetLeftMargin(0.15)
            canvas.SetRightMargin(0.15)
            
            canvas.Update()
            canvas.Draw()

            #display fit parameters
            text = ROOT.TLatex()
            text.SetNDC()
            text.SetTextFont(43)
            text.SetTextSize(20)

            text.DrawLatex(0.2, 0.9, "#chi^{2}/NDF:"+" {:.2f}/{}".format(fit_results.GetChisquare(),fit_results.GetNDF())+f" - Pixel: {pix}")
            l=set_label(name = "")
            
            # Save fig 
            canvas.SaveAs(f"{saveFig}/Linearity.p{pix}.png")

    return store_lsb

def toa_vs_tot(toa, tot, mcp, clock, pixels, saveFig=None, stats=150):   
    import_style() 
    for pix in np.unique(pixels):
        sel_p = (pixels == pix) & (tot!=0)
                
        if np.count_nonzero(sel_p)<stats:
            continue

        toa_p = toa[sel_p]
        tot_p = tot[sel_p]
        mcp_p = mcp[sel_p]
        clock_p = clock[sel_p]
        dt = clock_p-mcp_p

        if len(toa_p)<stats:
            continue

        totLimit_l, totLimit_h = tot_limits(tot_p, interval=(s.window_tot[0],s.window_tot[1]))
        cut = (mcp_p!=0) & ((-mcp_p+clock_p)<0) & (tot_p>=totLimit_l) & (tot_p<totLimit_h)

        canvas = ROOT.TCanvas("canvas", "", 800, 800)

        # Initiaslisation of histograms
        profile = ROOT.TProfile("", "", 128,-0.5 ,127.5, totLimit_l, totLimit_h)
        # hist2d = ROOT.TH2D("", "", 128,-0.5, 127.5, int((totLimit_h-totLimit_l)/50), totLimit_l, totLimit_h)
        
        # Fill histograms
        for i in range(len(tot_p[cut])):
            # hist2d.Fill((toa_p[cut])[i], (tot_p[cut])[i])
            profile.Fill((toa_p[cut])[i], (tot_p[cut])[i])
            
        # Do the fit
        if type(saveFig) == str:    
            # Draw
            # hist2d.Draw("COLZ")
            profile.Draw()
            # #set marker
            profile.SetMarkerStyle(20)
            profile.SetMarkerSize(1)
            
            # #axis labels
            profile.GetXaxis().SetTitle("#bf{TOA [DAC]}")
            profile.GetYaxis().SetTitle("#bf{TOT [ps]}")
            profile.GetYaxis().SetRangeUser(totLimit_l, totLimit_h)

            # # Add legend
            canvas.SetLeftMargin(0.15)
            canvas.SetRightMargin(0.15)
            
            canvas.Update()
            canvas.Draw()

            #display fit parameters
            text = ROOT.TLatex()
            text.SetNDC()
            text.SetTextFont(43)
            text.SetTextSize(20)

            text.DrawLatex(0.2, 0.9, f" Pixel: {pix}")
            l=set_label(name = "")
            
            # Save fig 
            canvas.SaveAs(f"{saveFig}/TOA_vs_TOT.p{pix}.png")

def smallest_index(row):

    nonzero_elements = row[np.all(row != 0)]
    if len(nonzero_elements) > 0:
        min_nonzero_index = np.argmin(nonzero_elements.flatten())
        return min_nonzero_index
    else:
        # If all elements are zero, return a special value (e.g., -1) to indicate no non-zero element
        return -1

def apply_tw_correction(dt, tot,  params):
    correction = [params[i]*tot**(i) for i in range(len(params))]
    dt_corrected = dt - np.sum(correction, axis=0)
    
    return dt_corrected  

def tot_limits(tot, interval=(3,4)):
    center = center_histogram(tot, resolution=s.totResolution, plot=False)
    totLimit_l = center - s.totResolution*interval[0]
    totLimit_h = center + s.totResolution*interval[1]
    
    return totLimit_l, totLimit_h

def optimize_resolution(toa, mcp, clock, lsb, toaSegments = [0,32,64,96, 128], totInterval = (3,4), saveFig=None, provideLSB=False):

    dt = -toa*lsb - (mcp - clock)
    center = center_histogram(dt)
    dt=dt-center
    
    params = lsb_interval(toa, mcp, clock, interval=toaSegments, provideLSB=provideLSB)    

    lsbs = params[:,2]
    centers = params[:,-1]
    toa_corrected, shifts = correct_toa(toa, lsbs ,toaSegments, centers)

    if type(provideLSB)==list:
        lsb_ = [1]
    else:
        lsb_ = np.linspace(.9, 1.1, 200)
    params_res, dt = best_resolution(toa_corrected-shifts, mcp, clock, np.unique(params[:,1]),  lsb_, saveFig=saveFig)
    
    best_dt_resolution = params_res[0]
    lsbs = lsbs*params_res[1]
    
    return [best_dt_resolution, lsbs, dt , params_res[2:4]]




def fit_constant(x, y, y_err=None):
    def constant_function(x, c):
        return c
    
    if len(y)==1:
        return y[0], y_err[0] if y_err is not None else 0, 0

    # Perform the fit
    if y_err is None:
        params, covariance = curve_fit(constant_function, x, y)
        chi2 = np.sum(((y - constant_function(x, *params)))**2)
    else:  
        params, covariance = curve_fit(constant_function, x, y, sigma=y_err, absolute_sigma=True)
        chi2 = np.sum(((y - constant_function(x, *params)) / y_err)**2)
    return params[0], np.sqrt(np.diag(covariance))[0], chi2

def remove0(arr):
    non_zero_rows = np.any(arr != 0, axis=1)
    return arr[non_zero_rows]

def toa_segments_resolution(toa, tot, mcp, clock, lsb, pixels,toaSegments=[0,32,64,96, 128], path_fig = '',  stats=150, provideLSB=False):
    '''
    Compute the LSB that minimize the resolution for each pixel in a specified interval of TOA
    [toaSements]: list of the intervals of TOA
    '''
    
    store_lsb = np.zeros((225, len(toaSegments)-1))
    store_reso = np.zeros((225, 2))
    store_tw = np.zeros((225, 2))
    
    for pix in np.unique(pixels):
        sel_p = (pixels == pix) & (tot!=0)

        if provideLSB is not False:
            # Conversion to list necessary for the well behaviour of functions
            lsb = provideLSB[pix,:].tolist()
        
        if np.count_nonzero(sel_p)<stats:
            continue


        toa_p = toa[sel_p]
        tot_p = tot[sel_p]
        mcp_p = mcp[sel_p]
        clock_p = clock[sel_p]
        lsb_p = lsb[sel_p]

        totLimit_l, totLimit_h = tot_limits(tot_p, interval=(s.window_tot[0],s.window_tot[1]))
        cut = (mcp_p!=0) & ((-mcp_p+clock_p)<0) & (tot_p>=totLimit_l) & (tot_p<totLimit_h)

        best_dt_resolution, lsbs, dt, reso = optimize_resolution(toa_p[cut], mcp_p[cut], clock_p[cut], lsb_p[cut],  toaSegments=toaSegments, provideLSB=provideLSB)
        binning = ( int((s.window_fit[1]-s.window_fit[0])/(best_dt_resolution)), s.window_fit[0], s.window_fit[1])
        fit_gaussian(dt, binning, saveFig=path_fig+f"/Resolution.p{pix}.png", infoFig=f"Pixel - {pix}")

        print(f"LSB: {[f'{l:.2f}' for l in lsbs]} \t Resolution: {reso[0]:.2f}+/-{reso[1]:.2f}")

        store_lsb[pix] = lsbs
        store_reso[pix] = reso
        
        totLimit_l, totLimit_h = tot_limits(tot_p, interval=(s.window_tot_tw[0],s.window_tot_tw[1]))
        binning_tot = (totLimit_l, totLimit_h, int((totLimit_h-totLimit_l)/s.totResolution))
        binning_dt = (s.window_fit[0], s.window_fit[1], int((s.window_fit[1]-s.window_fit[0])/(best_dt_resolution)))

        params = correct_time_walk(dt, tot_p[cut], binning_tot, binning_dt, pixel=pix, order=3, saveFig=f"{path_fig}/Time-Walk_Correction.p{pix}.png")
        if params == None:
            store_tw[pix] = [0,0]
        else:
            dt_corrected = apply_tw_correction(dt, tot_p[cut], params)
            res_tw = fit_gaussian(dt_corrected, binning, saveFig=None)[2]
        
        print(f" TW Correction : {res_tw[0]:.2f} +/- {res_tw[1]:.2f} ps")
        
        store_tw[pix] = res_tw
        rm_low = store_reso[:,0]<20
        store_reso[rm_low] = 0
        store_lsb[rm_low] = 0
        store_tw[rm_low] = 0
    
    return store_lsb, store_reso, store_tw

# ------------ D A T A B A S E ------------ 
def initialize_database(PATH_DATABASE):
    data = {}
    with open(PATH_DATABASE, 'w') as json_file:
        json.dump(data, json_file)


def get_database(PATH_DATABASE):
    with open(PATH_DATABASE) as json_file:
        return json.load(json_file)
    
def update_database(data, PATH_DATABASE):
    with open(PATH_DATABASE, 'w') as json_file:
        json.dump(data, json_file)

def get_pixels_values(path_database, BATCH, branch):
    store = []
    db = get_database(path_database)
    k = db[f'{BATCH}']['pixels'].keys()
    store = [db[f'{BATCH}']['pixels'][i][branch] for i in k]
    
    return np.concatenate(store)