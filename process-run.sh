#!/bin/bash
base=/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam
webCampaign=/eos/user/h/hgtdtb/www
DoProcess=""
doReco=0
processed=()
unprocessed=()

############################### PRINT DATE TIME ###############################
print_line() {
    local char="$1"
    local length="$2"
    printf "%s\n" "$(printf "%-${length}s" "$char" | tr ' ' "$char")"
}
datetime=$(date +"%A, %B %d, %Y %H:%M:%S")
length=${#datetime}
print_line "=" "$((length + 4))"
printf "| %s |\n" "$datetime"
print_line "=" "$((length + 4))"
##############################################################################
############################### CHECK CAMPAIGN ###############################
last_modified_dir=$(find "$base"*/ -maxdepth 1 -type d -exec stat --format='%Y %n' {} + 2>/dev/null | sort -n | tail -1 | awk '{print $2}')
CampaignName=`echo ${last_modified_dir} | sed "s:${base}::" | sed "s:/::"`

# Check if config.ini exists in the last modified directory
config_ini_path="$last_modified_dir/config.ini"

if [ -f "$config_ini_path" ]; then
    echo "Config.ini found in: $config_ini_path"
    DoProcess=$config_ini_path
    clock_channel=$(grep 'clock' $config_ini_path | awk -F'=' '{print $2}')
    mcp_channel=$(grep 'mcp' $config_ini_path | awk -F'=' '{print $2}')
    probe_channel=$(grep 'probe' $config_ini_path | awk -F'=' '{print $2}')

else
    # Find the second most recent directory
    second_last_modified_dir=$(find "$base"*/ -maxdepth 1 -type d -exec stat --format='%Y %n' {} + 2>/dev/nul | sort -n | tail -2 | head -n 1 | awk '{print $2}')
    CampaignName=`echo ${last_modified_dir} | sed "s:${base}::" | sed "s:/::"`

    # Check if config.ini exists in the second most recent directory
    config_ini_path="$second_last_modified_dir/config.ini"
    
    if [ -f "$config_ini_path" ]; then
        echo "Config.ini found in: $config_ini_path"
        DoProcess=$config_ini_path
    else
        echo "config.ini not found in the last two modified directories."
        echo ""
        echo " ---> Edit config.ini file in the campaign directory to allow runs to be processed"  
    fi
fi

if [ -n $DoProcess ];then
    echo ""
    echo "================================================"
    echo "     Campaign Name:     $CampaignName "
    echo "     Clock Channel:     $clock_channel"
    echo "     Probe Channel:     $probe_channel"     
    echo "     MCP Channel:       $mcp_channel"
    echo "================================================"

fi
##############################################################################

############################### AVAILABLE RUNs ###############################
numbers=$(find "${base}/${CampaignName}/Alvin" -type f -name "EUDAQ_SCAN_*.dat" | grep -oE 'EUDAQ_SCAN_([0-9]+)\.dat' | grep -oE '[0-9]+')
# Check each number
for number in ${numbers}; do
    # run=$(printf "%05d" "$number")
    run=${number}
    # Check if the file exists
    mergedFileName="${base}/${CampaignName}/Merged/Merged-${CampaignName}-${run}.root"
    if [ -f "${mergedFileName}" ];then
        processed+=("$run")
    else
        unprocessed+=("$run")
    fi
done
##############################################################################

######################### PROCESS RUN RECONSTRUCTION #########################
for run in ${unprocessed[@]}; do
    if [ ${run} -gt 450 ]; then
        srun="$run"
        if [[ ${run} -gt 0 ]];
            then
            run=$(printf "%05d" "$run")
            echo $run
        fi

        digitizer_file="${base}/${CampaignName}/Digitizer/run${run}/"
        # Check if the file or directory exists
        if [ -e $digitizer_file ]; then
            # If it exists, run the ls command
            echo ".wfm Files exist"
        else
            echo ".wfm Files do not exist"
            continue
        fi

        merged_path="${base}/${CampaignName}/Merged/Merged-${CampaignName}-${run}.root"
        if [ -e "${merged_path}" ]; then
            echo "Merged file exist for Run ${run}."
        else
            # Check if Digitizer is being written
            file_to_monitor="/afs/cern.ch/user/h/hgtdtb/Software/Scripts/OnlineMonitoring/reconstruction/log/jobs/job-Digitizer-${CampaignName}-${run}.err"
        if lsof -w "${file_to_monitor}" >/dev/null; then
                echo "--- Digitizer data being written"
                if (( SECONDS - start_time >= timeout )); then
                    doPlot=0
                    echo "--------> The timeout of $timeout seconds has been reached."
                    break
                fi
                sleep 10
            fi

            echo "-------------------------------------------------------"
            echo $nowh"-- Running process-run.sh"
            echo "-- Campaign name : "${CampaignName}" Run: "$run
            echo "-------------------------------------------------------"

            source ./reconstruction/doReconstruction.sh ${CampaignName} ${run}

            echo ""
            echo "- Start the files merging"

            source ./reconstruction/doMerging.sh ${CampaignName} ${run} ${clock_channel} ${probe_channel} ${mcp_channel}
            
            echo "-------> File ntuples/Merged-${CampaignName}-${run}.root created"
            echo "-------------------------------------------------------"
            echo ""
            echo "- Compute the LSB for each pixels available"

            python3 ./reconstruction/asic/quickLSB.py "${merged_path}"

            echo "----- LSB for the TOA added to the Merged file"
        
            # plot run 
            timeout=300
            start_time=$SECONDS
            doPlot=1

            echo '- Creation of plots'
            if [[ $doPlot -eq 1 ]];then
            #     cp /eos/user/h/hgtdtb/www/index-template.html /eos/user/h/hgtdtb/www/index.html 

                mkdir -p "${webCampaign}/Campaigns/${CampaignName}/figures/figures-${run}"
                python3 analysis/TimingDistributions.py "${base}/${CampaignName}/Alvin/ntuples/Alvin-${CampaignName}-${run}.root" ${run} ${CampaignName}  -t "tree"
                python3 analysis/HitsDistribution.py "${base}/${CampaignName}/Alvin/ntuples/Alvin-${CampaignName}-${run}.root" "${CampaignName}" "${run}"
                python3 analysis/SignalShapes.py "${base}/${CampaignName}/Digitizer/run${run}" $run $CampaignName --channels $clock_channel $mcp_channel $probe_channel
                python3 analysis/Occupancy.py "${base}/${CampaignName}/Alvin/ntuples/Alvin-${CampaignName}-${run}.root" $CampaignName $run 
                python3 analysis/Amplitudes.py "${base}/${CampaignName}/Digitizer/ntuples/Digitizer-${CampaignName}-${run}.root" $CampaignName $run
                python3 analysis/ComputeResolutions.py "${merged_path}" "${CampaignName}" "${run}"

            fi
        fi
    fi
done
echo ""

##############################################################################

##################################
####### TO PROCESS ONE RUN #######
##################################

# CampaignName="November2023"

# run="09820"
# check if merged file exists
# mergedFileName="ntuples/Merged-${CampaignName}-${run}.root"
# if [ -f "$mergedFileName" ]; then
#     doReco=0
#     echo "The file '$mergedFileName' exists."
# else 
#     doReco=1
#     echo "The file '$mergedFileName' does not exist."
# fi

# if [ $doReco -gt 0 ];then
#     echo "-------------------------------------------------------"
#     echo $nowh" - Running recoTB-tmp.sh"

#     echo "-- Campaign name : "${CampaignName}" Run: "$run

#     source /afs/cern.ch/user/h/hgtdtb/Software/Scripts/Cron/process-run-tmp.sh ${CampaignName} ${run}
#     sleep 10
#     source /afs/cern.ch/user/h/hgtdtb/Software/Scripts/Cron/merge-run.sh ${CampaignName} ${run}

# else
#     echo " - Nothing to process -- exit"
# fi
