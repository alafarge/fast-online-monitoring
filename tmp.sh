num=1100
CampaignName="November2023"
base=/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam

batch_file="${base}/${CampaignName}/Batch/ntuples/Batch-${CampaignName}-${num}.root"

root "${batch_file}" -b "./analysis/AnalysisSkew.C(-${num},\"${CampaignName}\")"
