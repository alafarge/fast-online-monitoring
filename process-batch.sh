#!/bin/bash

base=/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam
webCampaign=/eos/user/h/hgtdtb/www

# Specify the directory
last_modified_dir=$(find "$base"*/ -maxdepth 1 -type d -exec stat --format='%Y %n' {} + 2>/dev/null | sort -n | tail -1 | awk '{print $2}')
CampaignName=`echo ${last_modified_dir} | sed "s:${base}::" | sed "s:/::"`

# Specify the file number
batch_num=$(find "${base}/${CampaignName}/Batch/config" -type f -name "batch-*.txt" | grep -oE 'batch-([0-9]+)\.txt' | grep -oE '[0-9]+')

cp -f "${base}/${CampaignName}/Batch/config/batches_config.json" "${webCampaign}/Campaigns/${CampaignName}/batches_name.json"

declare -a line
for  num in ${batch_num};do
    batch_file="${base}/${CampaignName}/Batch/ntuples/Batch-${CampaignName}-${num}.root"
    # if [[ num  -ne 99 ]];then
    #     continue
    # fi

    echo " -- Batch ${num} is processing"
    lines=()
    if [[ -f "${batch_file}" ]];then
        echo "- Files already merged per batch"
    else 
        run_numbers="${base}/${CampaignName}/Batch/config/batch-${num}.txt"

        # Read the file line by line
        while IFS= read -r line
        do
            lines+=("$line") 
        done < "${run_numbers}"
        echo "${run_numbers[@]}"
        # Initialize hadd command
        echo $batch_file
        if [[ run_numbers > 0 ]];then 

            # rm batch file if it exists
            if [[ -f "${batch_file}" ]];then
                # rm "${batch_file}"
                continue    
            fi
            hadd_command="hadd -f ${batch_file}"

            # Add each ROOT file to the hadd command
            n_existing=0

            if [ "${#lines[@]}" -eq 1 ]; then
                cp "$source_file" "$destination_directory"
            fi

            for run in ${lines[@]};do
                run=$(printf "%05d" "$run")
                # Verify if file to merge exists
                if [[ -f "${base}/${CampaignName}/Merged/Merged-${CampaignName}-${run}.root" ]];then
                    hadd_command+="  ${base}/${CampaignName}/Merged/Merged-${CampaignName}-${run}.root"
                    ((n_existing=${n_existing}+1))
                else
                    echo "- File ${run} does not exist"
                fi
            done

            echo "${n_existing}"
        
            if [[ ${n_existing} -ne ${#lines[@]} ]];then
                continue
            fi

            mkdir -p "${webCampaign}/Campaigns/${CampaignName}/figures/figures--${num}"

            eval $hadd_command

            if [[ -f "${batch_file}" ]];then
                echo "- Batch ${num} merged"
            else
                echo "- Batch ${num} not merged" 
            fi
            sleep 1
            python3 analysis/ComputeResolutions.py "${batch_file}" "${CampaignName}" "-${num}"
            python3 analysis/TimingDistributions.py "${batch_file}" -${num} ${CampaignName}
            root "${batch_file}" -b "./analysis/AnalysisSkew.C(-${num},\"${CampaignName}\")"

        fi
    fi 
done
