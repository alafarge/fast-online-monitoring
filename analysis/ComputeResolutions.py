import argparse, sys

sys.path.append('./utils')
import utils as u
import numpy as np
import uproot as up
import plotter as p
import fill_lsb as l
import settings as s

parser = argparse.ArgumentParser(description='Compute the LSBs and the resolutions for a given batch')
parser.add_argument('PATH_DATA', type=str, help='Path to the data')
parser.add_argument('Campaign', type=str, help='Campaign name')
parser.add_argument('BATCH', type=str, help='Batch number')

global PATH_FIG
global PATH_DATABASE

def lsb_computer(PATH_DATA, Campaign, BATCH, stats=150, ):
    print(PATH_DATA)
    tree = up.open(PATH_DATA)['Sequoia']
    # variables
    toa = -tree['tTOA'].array(library='np')
    tot = tree['tTOT'].array(library='np')
    mcp = tree['tMCP'].array(library='np')
    clock = tree['tClock'].array(library='np')
    lsb = tree['lsbTOA'].array(library='np')
    col = tree['nCol'].array(library='np')
    row = tree['nRow'].array(library='np')

    pixels = u.to_pixel(col, row)
    # computations
    if BATCH<0:
        store_lsb, store_reso, store_tw= u.toa_segments_resolution(toa, tot, mcp, clock, lsb, pixels, toaSegments=[0,32,64,96, 128], path_fig=PATH_FIG, stats=stats)
    store_lsb_single, store_reso_single, store_tw_single = u.toa_segments_resolution(toa, tot, mcp, clock, lsb, pixels, toaSegments=[0,127] , path_fig=PATH_FIG, stats=stats)
    # plots

    if np.all(store_lsb_single == 0):
        return 0

    if BATCH<0:
        p.lsbs_plot(store_lsb, f"{Campaign} - b{BATCH}", saveFig=f"{PATH_FIG}/LSBs_Distribution_4_Cycles.png")
        p.resolutions_plot(store_reso[:,0], store_reso[:,1], f"{Campaign} - b{BATCH}", saveFig=f"{PATH_FIG}/Distribution_of_Resolutions_4_Cycles.png")
    else: 
        p.lsbs_plot(store_lsb_single, f"{Campaign} - r{BATCH}", saveFig=f"{PATH_FIG}/LSBs_Distribution_1_Cycle.png")
        p.resolutions_plot(store_reso_single[:,0], store_reso_single[:,1], f"{Campaign} - r{BATCH}", saveFig=f"{PATH_FIG}/Distribution_of_Resolutions_1_Cycle.png")
    
    u.linearity(toa, tot, mcp, clock, pixels, saveFig=PATH_FIG, stats=stats)
    u.toa_vs_tot(toa, tot, mcp, clock, pixels, saveFig=PATH_FIG, stats=stats)
    
    if BATCH<0:
        p.Occupancy(col,row, saveFig=PATH_FIG)

    if BATCH<0:
        return [store_lsb, store_reso, store_tw, store_lsb_single, store_reso_single, store_tw_single]
    else:
        return [store_lsb_single, store_reso_single, store_tw_single]

def fill_database_pixels(path_database,path_data,  data, var_names, BATCH):
    '''
    Fill  a database from the data. The database has to be initialized 1st.
    
    [path_database]: str, path to the database for the respective campaign
    [data]:          list of arrays of the format (225,n), data to be stored
    [var_names]:     list of str, names of the variables associated with the data
    '''
    db = u.get_database(path_database)
    pix = np.where(np.any(np.hstack(data)!=0, axis=1))[0]
    pix_data = {int(i): {var_names[j]: list(data[j][i,:]) for j in range(len(data))} for i in pix}
    db[BATCH] = {'pixels': pix_data}
    db[BATCH]['entries'] = u.get_entries(path_data)

    u.update_database(db, path_database)

def fill_database_entry(path_database, data, var_names, BATCH):
    db = u.get_database(path_database)
    for i in range(len(var_names)):
        db[BATCH][f'{var_names[i]}'] = data[i]

    u.update_database(db, path_database)

def fill_database_fit(path_database, var_names, BATCH):

    for i in range(len(var_names)):
        v = u.get_pixels_values(path_database, f'{BATCH}', var_names[i])
        y = v[::2]
        y_err = v[1::2]
        rm = np.where(y_err==0)[0]
        y = np.delete(y, rm)
        y_err = np.delete(y_err, rm)
        x=np.arange(len(y))

        if len(y)==0:
            continue
        elif len(y)==1:
            fill_database_entry(path_database, [[y[0], y_err[0]]], [var_names[i]], f'{BATCH}')
        else:
            fit_val = [u.fit_constant(x, y, y_err=y_err)][:2]
            fill_database_entry(path_database, fit_val, [var_names[i]], f'{BATCH}')

def fill_database_best_fit(val, val_u, BATCH, path_database):
    result_indices = np.apply_along_axis(u.smallest_index, axis=1, arr=val)
    pass_pixels = val[np.where(result_indices>=0)[0],:]
    pass_pixels_u = val_u[np.where(result_indices>=0)[0],:]

    y =np.array([pass_pixels[ith, i] for ith, i in enumerate(result_indices[result_indices>=0])])
    y_err =np.array([pass_pixels_u[ith, i] for ith, i in enumerate(result_indices[result_indices>=0])])
    gt0 = y_err>0
    y = y[gt0]
    y_err = y_err[gt0]
    x=np.arange(len(y))

    if len(y)==0:
        pass
    elif len(y)==1:
        fill_database_entry(path_database, [[y[0], y_err[0]]], ['best_fit'], f'{BATCH}')
    else:
        fit_val = [u.fit_constant(x, y, y_err=y_err)][:2]
        print(fit_val)
        fill_database_entry(path_database, fit_val, ['best_fit'], f'{BATCH}')

if __name__=='__main__':
    args = parser.parse_args()
    PATH_DATA = args.PATH_DATA
    Campaign = args.Campaign
    BATCH = args.BATCH
    figures_dir = f'figures-{BATCH}'

    if int(BATCH)>0:
        database_name = 'database-run.json'
        stats = s.stats_run
    else:
        database_name = 'database.json'
        stats = s.stats_batch
    
    BATCH = int(BATCH)
    PATH_FIG = f"/eos/user/h/hgtdtb/www/Campaigns/{Campaign}/figures/{figures_dir}"
    PATH_DATABASE = f'/eos/user/h/hgtdtb/www/Campaigns/{Campaign}/{database_name}'
    
    data = lsb_computer(PATH_DATA, Campaign, BATCH, stats=stats)
    if data == 0:
        print('No data to process')
        sys.exit(0)

    # Fill BATCH
    if BATCH<0:

        # Fill the database
        var_names = ['4lsb', '4resolution', '4tw', '1lsb', '1resolution', '1tw']
        fill_database_pixels(PATH_DATABASE, PATH_DATA, data, var_names, -BATCH)
        fill_database_fit(PATH_DATABASE, ['4resolution', '4tw', '1resolution', '1tw'], -BATCH)
        
        # Optimize fit result between w and w/o TW correction
        res = data[1]
        res_tw = data[2]
        val= np.vstack([res[:,0], res_tw[:,0]]).T
        val_u = np.vstack([res[:,1], res_tw[:,1]]).T
        fill_database_best_fit(val, val_u, -BATCH, PATH_DATABASE)

        #Fill the number of pixels reconstructed
        fill_database_entry(PATH_DATABASE, [res.shape[0]], ['nPixels'],  f'{BATCH}')

    # Fill RUN
    else:
        var_names = ['1lsb', '1resolution', '1tw']
        fill_database_pixels(PATH_DATABASE, PATH_DATA, data, var_names, BATCH)
        fill_database_fit(PATH_DATABASE, ['1resolution', '1tw'], BATCH)
        
        # Optimize fit result between w and w/o TW correction
        res = data[1]
        res_tw = data[2]
        val= np.vstack([res[:,0], res_tw[:,0]]).T
        val_u = np.vstack([res[:,1], res_tw[:,1]]).T
        fill_database_best_fit(val, val_u, BATCH, PATH_DATABASE)

        #Fill the number of pixels reconstructed
        fill_database_entry(PATH_DATABASE, [res.shape[0]], ['nPixels'],  f'{BATCH}')

    #Add LSB to the tree
    # lsb = (data[0])[np.all(data[0]!=0, axis=1)]
    # l.update_lsb(PATH_DATA, data[0], toaSegments=[0,32,64,96, 128])
