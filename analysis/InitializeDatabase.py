import json, argparse, sys
sys.path.append('./utils')
from utils import initialize_database


parser = argparse.ArgumentParser(description='Initialize the database')
parser.add_argument('path_database', type=str, default='Campaign', help='Name of the database')

if __name__ == "__main__":
    args = parser.parse_args()
    initialize_database(args.path_database)
    