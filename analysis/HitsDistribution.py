import numpy as np
import matplotlib.pyplot as plt
import os, sys, re, argparse
import uproot as up
sys.path.append('./utils')
import plotter as p

parser = argparse.ArgumentParser(description='plot Distribution o the number of hits  per event')
parser.add_argument('alvin_file', metavar='alvin_file', type=str, help='Alvin file name .root')
parser.add_argument('campaign_name', metavar='campaign_name', type=str, help='The name of the campaign')
parser.add_argument('run_number', metavar='run_number', type=str, help='RUN Number')

global PATH_FIG

def HitsDistribution(file_name, RUN, Campaing):
    tree = up.open(file_name)['tree']
    nHits = tree['nhits'].array(library='np')
    fig, ax = p.myPlot(xlabel='Number of Hits', ylabel='Events', comment=f'RUN {RUN}')
    min_r = -0.5
    max_r = np.max(nHits)+0.5
    nbin = int(max_r-min_r)
    ax[0].hist(nHits, bins=nbin,range=(min_r, max_r),  linewidth=2, label='Data', histtype='bar', color='steelblue')    
    ax[0].set_ylim(0, 1.1*np.max(ax[0].get_ylim()))

    fig.savefig(f'{PATH_FIG}/Number_of_Hits_Distribution.png', bbox_inches='tight')

if __name__=='__main__':
    args = parser.parse_args()
    file_name=args.alvin_file
    run=args.run_number
    Campaign=args.campaign_name
    PATH_FIG = f"/eos/user/h/hgtdtb/www/Campaigns/{Campaign}/figures/figures-{run}"

    HitsDistribution(file_name,run, Campaign)