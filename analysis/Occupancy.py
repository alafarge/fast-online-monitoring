import argparse, sys
sys.path.append('./utils')
import utils as u
import numpy as np
import uproot as up
import plotter as p
import fill_lsb as l
import settings as s

global PATH_FIG

parser = argparse.ArgumentParser(description='Produce the plot of the occupancy map')
parser.add_argument('PATH_DATA', metavar='alvin_file', type=str, help='Alvin file name .root')
parser.add_argument('campaign_name', metavar='campaign_name', type=str, help='The name of the campaign')
parser.add_argument('run_number', metavar='run_number', type=str, help='RUN Number')
   
def occupancyMap(PATH_DATA, run_number, campaign_name):

    tree = up.open(PATH_DATA)['tree']
    # variables
    col = np.concatenate(tree['col'].array(library='np'))
    row = np.concatenate(tree['row'].array(library='np'))
    
    pixels = u.to_pixel(col, row)

    p.Occupancy(col,row, saveFig=PATH_FIG)

if __name__=='__main__':
    args = parser.parse_args()
    PATH_DATA = args.PATH_DATA
    Campaign = args.campaign_name
    BATCH = args.run_number

    PATH_FIG = s.PATH_FIG.format(Campaign, BATCH)
    occupancyMap(PATH_DATA, Campaign, BATCH)