import ROOT 
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os, re
import warnings
import uproot as up
warnings.filterwarnings("ignore")

parser = argparse.ArgumentParser(description='plot TOA/TOT distributions')
parser.add_argument('alvin_file', metavar='alvin_file', type=str, help='Alvin file name .root')
parser.add_argument('run_number', metavar='run_number', type=str, help='RUN Number')
parser.add_argument('campaign_name', metavar='campaign_name', type=str, help='The name of the campaign')
parser.add_argument('-t','--tree' , default='Sequoia', type=str, help='The name of the tree')

global PATH_FIG
global tree_name

def extract_pixel_number(filename):
    # Define a regular expression pattern to match the number before ".csv"
    pattern = r'(\d+)(?=\.\w+$)'
    match = re.search(pattern, filename)

    if match:
        # Extract the matched number
        number = match.group(1)
        return int(number)
    else:
        # Return an appropriate value if no match is found
        return None
    
def list_files_params(folder_path):
    files = os.listdir(folder_path)
    matching_files = [file for file in files if file.startswith("tw_parameters")]
    numbers = [extract_pixel_number(file) for file in matching_files]

    return numbers

def get_params(folder_path):
    params = np.zeros((0,3))
    pixels = list_files_params(folder_path)
    for p in pixels:
        file_path = os.path.join(folder_path, f"tw_parameters_{p}.csv")
        data = np.loadtxt(file_path, delimiter=",")[0::2]
        params = np.vstack((params, data))
    return [params, pixels]

def to_pixel(col, row):
    if type(col) != type(np.zeros(1)):
        col = np.array(col)
        row = np.array(row)
    pixels = col*15+row
    return pixels

def to_col_row(pixels):
    if type(pixels) != type(np.zeros(1)):
        pixels = np.array(pixels)
    col = pixels//15
    row = pixels%15
    
    return col, row

def timing_distribution(PATH_DATA, name):
    
    tree = up.open(PATH_DATA)[tree_name]
    # variables
    if tree_name == 'Sequoia':
        toa_b = 'tTOA'
        tot_b = 'tTOT'
    elif tree_name == 'tree':
        toa_b = 'toa'
        tot_b = 'tot'

    toa = -tree[toa_b].array(library='np')
    tot = tree[tot_b].array(library='np')
    
    if tree_name=='tree':
        toa = np.concatenate(-toa)
        tot = np.concatenate(tot)

    mask_toa = (toa < 127) & (toa > 0)
    mask_tot = (tot > 0)

    saturated = np.count_nonzero(toa == 127)/len(toa)*100
    toa0 = np.count_nonzero(toa == 0)/len(toa)*100
    tot0 = np.count_nonzero(tot == 0)/len(tot)*100
    #plot toa
    fig, (ax, ax2) = plt.subplots(1, 2, figsize=(10, 5))
    ax.hist(toa[mask_toa], bins=128, range=(0,127), color='steelblue', linewidth=1)
    plot_type=r"$\rm{\it{"+name+r"}}$"
    ax.set_xlabel('TOA [DAC]', fontsize=9,fontweight='bold')
    ax.set_ylabel('Counts', fontsize=9,fontweight='bold')
    ax.set_title('Unsaturated TOA distribution')
    ax.text(0.03, 0.91, 'Saturated: {:.2f}%'.format(saturated), transform=ax.transAxes, va='top')
    ax.text(0.03, 0.87, 'TOA=0: {:.2f}%'.format(toa0), transform=ax.transAxes, va='top')
    ax.text(0.03, 0.97, 'HGTD Test Beam '+plot_type, transform=ax.transAxes,fontsize=12, va='top', fontweight='bold', fontstyle='italic')
    ax.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True)
    ax.set_ylim(0, 1.08*np.max(ax.get_yticks()))

    range_bins = (np.min(tot[mask_tot]),np.percentile(tot[mask_tot], 99))
    bins = int(((np.percentile(tot[mask_tot], 99)-np.min(tot[mask_tot]))/160)+1) 
    hist_tot= ax2.hist(tot[mask_tot], bins=bins, range=range_bins, color='steelblue', linewidth=1)
    ax2.set_xlabel('TOT [ps]', fontsize=9,fontweight='bold')
    ax2.set_ylabel('Counts', fontsize=9,fontweight='bold')
    ax2.set_title('TOT distribution')
    ax2.text(0.03, 0.91, 'TOT=0: {:.2f}%'.format(tot0), transform=ax2.transAxes, va='top')
    ax2.text(0.03, 0.97, 'HGTD Test Beam '+plot_type, transform=ax2.transAxes,fontsize=12, va='top', fontweight='bold', fontstyle='italic')
    ax2.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True)
    ax2.set_ylim(0, 1.08*np.max(ax2.get_yticks()))
    # add line for the max bin value and value
    max_tot = np.argmax(hist_tot[0])
    ax2.axvline(hist_tot[1][max_tot]+(160/2) , color='dimgrey', linestyle='--', linewidth=0.5)
    ax2.text(hist_tot[1][max_tot]+(160/2),ax2.get_ylim()[1]/3 , '{:.0f}'.format(hist_tot[1][max_tot]), color='dimgrey', verticalalignment='center', horizontalalignment='center', rotation=90, fontweight='bold')


    plt.tight_layout()
    fig.savefig(f'{PATH_FIG}/TOA_and_TOT_Distributions.png', bbox_inches='tight')
    
if __name__=='__main__':
    args = parser.parse_args()
    file_name=args.alvin_file
    run=args.run_number
    Campaign=args.campaign_name
    tree_name=args.tree
    
    PATH_FIG = f"/eos/user/h/hgtdtb/www/Campaigns/{Campaign}/figures/figures-{run}"

    timing_distribution(file_name, Campaign)