import numpy as np
import matplotlib.pyplot as plt
import argparse
import uproot as up
import os, re, sys
sys.path.append('./utils')
import plotter as p
import settings as s
import tekwfm
import configparser

parser = argparse.ArgumentParser(description='plot signal shapes')
parser.add_argument('DATA_PATH', metavar='DATA_PATH', type=str, help='name of digitizer file')
parser.add_argument('campaign_name', metavar='campaign_name', type=str, help='The name of the campaign')
parser.add_argument('run_number', metavar='run_number', type=str, help='RUN Number')

def get_amplitudes(DATA_PATH, campaign_name):
    config = configparser.ConfigParser()
    
    config.read(f'{s.DISK}/{campaign_name}/config.ini')

    clock_ch = int(config.get('Channels', 'clock'))-1
    mcp_ch = int(config.get('Channels', 'mcp'))-1
    probe_ch = int(config.get('Channels', 'probe'))-1

    tree=up.open(DATA_PATH)['tree']
    # variables

    clock = tree[f'CLK'].array(library='np')
    mcp = tree[f'Amax'].array(library='np')
    probe = tree[f'Amax'].array(library='np')

    clock = np.array([clock[i][clock_ch] for i in range(len(clock))])
    clock = clock[clock<0]
    mcp = [(mcp[i])[mcp_ch] for i in range(len(mcp))]
    probe = [(probe[i])[probe_ch] for i in range(len(probe))]

    return clock, mcp, probe


if __name__=='__main__':
    args = parser.parse_args()
    DATA_PATH = args.DATA_PATH
    run_number = args.run_number
    campaign_name = args.campaign_name

    fig, axs = p.signalPlot(comment=f'RUN {run_number}')
    axs[0].set_ylabel('Entry' ,weight='bold', fontsize=12)
    channels_amplitudes= get_amplitudes(DATA_PATH, campaign_name)
    for ith, ch in enumerate(['Clock', 'MCP', 'Probe']):
        ax=axs[ith]
        ax.set_xlabel(f'Amplitudes (V)', weight='bold', fontsize=12)
        min_r = np.min(channels_amplitudes[ith])
        max_r = np.quantile(channels_amplitudes[ith], .99)
        bins = np.linspace(min_r, max_r, 60)
        ax.hist(channels_amplitudes[ith],  bins=bins, range=(min_r, max_r),color='dimgrey', alpha=1, linewidth=1, label=f'{ch}')
        ax.legend()

    fig.savefig(f'{s.PATH_FIG.format(campaign_name, run_number)}/Amplitudes.png', bbox_inches='tight')

    
        
