import ROOT 
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os, re
import warnings
import uproot as up
warnings.filterwarnings("ignore")

parser = argparse.ArgumentParser(description='plot TOA/TOT distributions')
parser.add_argument('alvin_file', metavar='alvin_file', type=str, help='Alvin file name .root')
parser.add_argument('campaign_name', metavar='campaign_name', type=str, help='The name of the campaign')
parser.add_argument('run_number', metavar='run_number', type=str, help='RUN Number')

