import ROOT 
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os, re, sys
sys.path.append('./utils')
import plotter as p
import settings as s
import tekwfm


parser = argparse.ArgumentParser(description='plot signal shapes')
parser.add_argument('folder', metavar='folder', type=str, help='Digitizer folder name .wfm')
parser.add_argument('run_number', metavar='run_number', type=str, help='RUN Number')
parser.add_argument('campaign_name', metavar='campaign_name', type=str, help='The name of the campaign')
parser.add_argument('--channels', metavar='channel_number', nargs="+", type=int, help='channel')

def extract_numbers(folder_path):
    tek_numbers = []
    ch_numbers = []

    # Iterate through all files in the specified folder
    for filename in os.listdir(folder_path):
        # Ensure only files are considered
        if os.path.isfile(os.path.join(folder_path, filename)):
            # Use regular expressions to extract numbers after "Tek" and "ch"
            tek_match = re.search(r'Tek(\d+)', filename)
            ch_match = re.search(r'_ch(\d+)', filename)
            
            # If both matches are found, append the numbers to the respective lists
            if tek_match:
                tek_numbers.append(tek_match.group(1))
            if ch_match:
                ch_numbers.append(ch_match.group(1))

    return np.unique(tek_numbers).astype(int), np.unique(ch_numbers).astype(int)

def get_signals(file_path, n=10):
    signals=[]

    volts, tstart, tscale, tfrac, tdatefrac, tdate = tekwfm.read_wfm(file_path)   
    toff = tfrac * tscale
    samples, frames = volts.shape
    tstop = samples * tscale + tstart
    t = np.linspace(tstart+toff, tstop+toff, num=samples, endpoint=False) 
    
    n= t.shape[1] if n>t.shape[1] else n
    n = np.random.choice(t.shape[1], n, replace=False)
    # sel period
    for i in n:       
        t_c = t[:,i]
        volts_c = volts[:,i]
        signals.append([t_c, volts_c])
        # plot 

    return signals

if __name__=='__main__':
    args = parser.parse_args()
    dir_path = args.folder
    run_number = args.run_number
    campaign_name = args.campaign_name
    ch_numbers = args.channels
    tek_numbers, _ = extract_numbers(dir_path)

    fig, axs = p.signalPlot(comment=f'RUN {run_number}')
    for ith, ch in enumerate(ch_numbers):
        ax=axs[ith]
        ax.set_xlabel(f'Channel {ch}', weight='bold', fontsize=12)

        maxSig = np.min((50, len(tek_numbers)))
        for tek in tek_numbers[:maxSig]:
            file_path = os.path.join(dir_path, f'Tek000{tek}_ch{ch}.wfm')
            # select 5 signal shapes
            signals = get_signals(file_path, n=5)
            for signal in signals:
                ax.plot(signal[0], signal[1], color='dimgrey', alpha=0.1, linewidth=0.7)
    
    fig.savefig(f'{s.PATH_FIG.format(campaign_name, run_number)}/Signal_shapes.png', bbox_inches='tight')

    
        
