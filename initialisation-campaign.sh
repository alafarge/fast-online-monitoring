#!/bin/bash

base=/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam
webCampaign=/eos/user/h/hgtdtb/www

# last_modified_folder=$(find "$base" -type d -regex '.*/[0-9]\{4\}$' -print0 | xargs -0 ls -lt | head -n1 | awk '{print $NF}')


# # Specify the directory
last_modified_dir=$(find "$base"*/ -maxdepth 1 -type d  -exec stat --format='%Y %n' {} + 2>/dev/null | sort -n | tail -1 | awk '{print $2}')
CampaignName=`echo ${last_modified_dir} | sed "s:${base}::" | sed "s:/::"`
echo $CampaignName
if [[ $CampaignName =~ [0-9]{4}$ ]]; then

    campaign_path="/eos/user/h/hgtdtb/www/Campaigns/${CampaignName}/"

    if test -e "$campaign_path"; then
        echo "File exists: $campaign_path"
    else
        echo "- Initialisation of the campaign"
        mkdir -p "$campaign_path"
        mkdir -p "${campaign_path}/figures"
        python3 analysis/InitializeDatabase.py "${campaign_path}/database.json"
        python3 analysis/InitializeDatabase.py "${campaign_path}/database-run.json"
        echo "- Web initialisation done"
        mkdir -p "${base}/${CampaignName}/Merged"
        mkdir -p "${base}/${CampaignName}/Batch/ntuples"
        mkdir -p "${base}/${CampaignName}/Batch/config"
        mkdir -p "${base}/${CampaignName}/Alvin/ntuples"
        mkdir -p "${base}/${CampaignName}/Digitizer/ntuples"

        config_file="${base}/${CampaignName}/config.ini"
        echo "[General]" >> "$config_file"
        echo "name = ${CampaignName}" >> "$config_file"
        echo "" >> "$config_file"
        echo "[Channels]" >> "$config_file"
        echo "clock = 5" >> "$config_file"
        echo "mcp = 4" >> "$config_file"
        echo "probe = 1" >> "$config_file"
        echo "- Config file '$config_file' created."

        batch_config="${base}/${CampaignName}/Batch/config/batches_config.json"
        echo "{NUMBER : 'Settings_towards_sub_35_ps' } " >> "$batch_config"

    fi
fi