import ROOT
import numpy as np
import os, sys, re
curpath = os.getcwd()
sys.path.insert(0,curpath)
from array import array
import argparse
import time
import inspect
import cppyy
from copy import deepcopy
import ROOT
    
parser = argparse.ArgumentParser(description='Compute LSB from deltaT')
parser.add_argument('merged_file', metavar='merged_file', type=str, help='Merged file name .root')

def ExtractValuesOfBranch(fileName, treeName, branchName, varType="double", tcut=""):

    ROOT.gInterpreter.Declare("""
    #include <TFile.h>
    #include <TTree.h>

    std::vector<double> extract_branch_values_d(TFile* file, const char* treeName, const char* branchName, const char* tCut="") {
        TTree *tree = (TTree*)file->Get(treeName);
        std::vector<double> values;
        double value;
        TBranch *branch = tree->GetBranch(branchName);
        branch->SetAddress(&value);
        
        // Compile the TCut expression if provided
        TTreeFormula* cut = nullptr; 
        if(strlen(tCut) > 0){
            cut = new TTreeFormula("cut", tCut, tree);
        }
  
        for (int i=0; i<tree->GetEntries(); ++i) {
            branch->GetEntry(i);
            if(cut != nullptr && !cut->EvalInstance()) continue; // If a cut is defined, apply it on the fly
            if (value != -999) {
                values.push_back(value);
                }
        }

        if(cut != nullptr) delete cut; // cleanup
        return values;
    }

    std::vector<int> extract_branch_values_i(TFile* file, const char* treeName, const char* branchName) {
        TTree *tree = (TTree*)file->Get(treeName);
        std::vector<int> values;
        int value;
        TBranch *branch = tree->GetBranch(branchName);
        branch->SetAddress(&value);
        
        for (int i=0; i<tree->GetEntries(); ++i) {
            branch->GetEntry(i);
            if (value != -999) {
                values.push_back(value);
                }
        }

        return values;
    }
    """)
    inFile = ROOT.TFile(fileName)
    
    if varType == "double":
        values = ROOT.std.vector('double')(ROOT.extract_branch_values_d(inFile, treeName, branchName, tCut=tcut))
    elif varType == "int":
        values = ROOT.std.vector('int')(ROOT.extract_branch_values_i(inFile, treeName, branchName))        
    
    return np.array(values)

def to_pixel(col, row):
    if type(col) != type(np.zeros(1)):
        col = np.array(col)
        row = np.array(row)
    pixels = col*15+row
    return pixels

def getLSBs(fileName):
        
    toa = ExtractValuesOfBranch(fileName, "Sequoia", "tTOA")
    mcp = ExtractValuesOfBranch(fileName, "Sequoia", "tMCP")
    clock = ExtractValuesOfBranch(fileName, "Sequoia", "tClock")
    tot = ExtractValuesOfBranch(fileName, "Sequoia", "tTOT")
    col = ExtractValuesOfBranch(fileName, "Sequoia", "nCol", varType='int')
    row = ExtractValuesOfBranch(fileName, "Sequoia", "nRow", varType='int')
    
    pixels=to_pixel(col, row)
    deltaT = toa-mcp+clock
    lsb_test = np.arange(15, 24, 0.005)
    
    pixels_u = np.unique(pixels)
    lsb_d = {}
    
    for p in pixels_u:
        mask = (toa != 0) & (tot != 0) & (pixels == p) & (mcp != 0) & ((-mcp + clock) < 0) & (tot > 2000) & (tot < 40000)
        
        if np.count_nonzero(mask)<100:
            lsb_d[p]=18
            continue
        
        sel_time = deltaT[mask]
        min_lsb=0
        
        for lsb in lsb_test:
            deltaT = (toa*lsb)-mcp+clock
            deltaT = deltaT[mask]
            val, bins = np.histogram(deltaT)
            peak = np.argmax(val)
            val, bins = np.histogram(deltaT, bins=250, range=(bins[peak+0],bins[peak+1]))
            # print(val)
            rms = np.max(val)
            if rms>min_lsb:
                min_lsb=rms
                lsb_d[p]=lsb
        
        deltaT = (toa*lsb_d[p])-mcp+clock
        val, bins = np.histogram(deltaT, bins=80)
        peak = np.argmax(val)

        # h = ROOT.TH1F('h', 'Histogram of Selected Time', 80,bins[peak] , bins[peak+1])
        # for i in range(len(deltaT)):
        #     h.Fill(deltaT[i])
            
        # c = ROOT.TCanvas('c', 'Canvas', 800, 600)
        # h.Draw()
        # c.SaveAs(f"./fig/time_histogram_{p}.png")
                        
    return lsb_d

# def add_lsb(file_name, lsb):
#     root_file = ROOT.TFile(file_name,'update')
#     tree = root_file.Get("Sequoia")

#     lsbTOA = array('d', [0])
#     lsb_branch = tree.Branch('lsbTOA1', lsbTOA, 'lsbTOA1/D')
    
#     for i in range(tree.GetEntries()):
#         tree.GetEntry(i)
#         pixel=to_pixel([tree.nCol], [tree.nRow])
#         lsbTOA[0] = lsb[pixel[0]]
#         lsb_branch.BackFill()
        
#     tree.Write("", ROOT.TObject.kOverwrite)
#     root_file.Delete()

                        
#     return lsb_d



def add_lsb(file_name, lsb):
    root_file = ROOT.TFile(file_name,'update')
    tree = root_file.Get("Sequoia")

    # add lsb value to  a tree
    lsbTOA = array('d', [0])
    lsb_branch = tree.Branch('lsbTOA', lsbTOA, 'lsbTOA/D')
    
    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        pixel=to_pixel([tree.nCol], [tree.nRow])
        lsbTOA[0] = lsb[pixel[0]]
        lsb_branch.BackFill()
        
    tree.Write("",ROOT.TObject.kOverwrite)
    root_file.Close()
    print("Finished adding LSB to the tree")
            
if __name__ == "__main__":
    ROOT.gROOT.SetBatch(ROOT.kFALSE)
    args = parser.parse_args()
    merged_f=args.merged_file
    lsb = getLSBs(merged_f)
    add_lsb(merged_f, lsb)
    
    
    

    
    
    