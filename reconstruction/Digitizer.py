import time
import os, re, sys
import numpy as np

sys.path.append("./utils")
import tekwfm
import ROOT
from array import array

doPlot = 0


def list_files_in_repository(repo_path):
    files_list = []
    for root, dirs, files in os.walk(repo_path):
        for file in files:
            files_list.append(os.path.join(root, file))
    return files_list


def get_campaign(path):
    match = re.search(r"/testbeam/([^/]*)", path)
    if match:
        return match.group(1)
    return None


def get_channels(file_list):
    ch = []
    for file in file_list:
        match = re.search(r"Tek\d+_ch(\d+).wfm", file)
        if match:
            ch.append(match.group(1))
    return np.unique(ch).astype(int)


def getalvinTOT():
    file_path = "/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam/October2023/Alvin/EUDAQ_SCAN_9482.dat"
    tot = 0
    with open(file_path, "r") as file:
        for line in file:
            values = line.strip().split()
            if int(values[0]) == 0:
                tot = int(values[3])
            else:
                continue
    return tot


def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split("([0-9]+)", key)]
    return sorted(l, key=alphanum_key)


def Converter(runNum, cycleNum, chNum, branches):

    Transimpedance = 4.700  # Transimpedance: 4700 mV * ps/fC

    starttime = time.time()
    """plot frame 0 vs all frames on a two plot figure with locked time axes"""
    datapath = "{}/run{:05d}/Tek000{:d}_ch{:d}.wfm".format(
        dataRoot, runNum, cycleNum, chNum
    )

    print("reading data:", datapath)

    volts, tstart, tscale, tfrac, tdatefrac, tdate = tekwfm.read_wfm(datapath)

    voltsAbs = volts * -1.0
    # print trigger time stamps
    # print(" 1 [%.2f]" % (time.time()-starttime))
    # for frame, utcseconds in enumerate(tdate):
    #    print('frame: {:02d}, trigger: {}'.format(frame, time.ctime(utcseconds)))
    # create time vector
    samples, frames = volts.shape
    tstop = samples * tscale + tstart
    # t = np.linspace(tstart, tstop, num=samples, endpoint=False)
    # print(" 2 [%.2f]" % (time.time()-starttime))
    t = np.linspace(tstart, tstop, num=samples, endpoint=False)
    # print(" 3 [%.2f]" % (time.time()-starttime))
    # fractional trigger
    times = np.zeros(volts.shape)
    for frame, subsample in enumerate(tfrac):
        toff = subsample * tscale
        times[:, frame] = t + toff
        # print("frame [%.2f], subsample [%.2f]"%(frame, subsample))
    tDecodingEnd = time.time()
    print(" [*] Decoding done... takes [%.3fs]" % (tDecodingEnd - starttime))
    # plot: fastframe

    # timeForNoise = 20 * 1e-9
    timeForNoise = 2 * 1e-9
    nSamplesForNoise = int(timeForNoise / tscale)
    # print(nSamplesForNoise)
    global doPlot, countlimit, count

    if doPlot:
        plt.figure(2)
        ax1 = plt.subplot(2, 1, 1)
        # ax1.axvline(linewidth=1, color='r') # trigger annotation
        ax1.plot(times[:, 0], volts[:, 0])
        plt.ylabel("volts (V)")
        plt.title("only frame 1")
        ax2 = plt.subplot(2, 1, 2, sharex=ax1)
        # ax2.axvline(linewidth=1, color='r') # trigger annotation
        ax2.plot(times, volts)
        plt.xlabel("time (s)")
        plt.ylabel("volts (V)")
        plt.title("all frames")

    tPlottingEnd = time.time()
    # print(" 4 [%.2f]" % (time.time()-starttime))

    print(" [*] Ploting done... takes [%.3fs]" % (tPlottingEnd - tDecodingEnd))

    Nwfm = 0
    cycleID = "{:05d}_c{:04d}_ch{:d}".format(runNum, cycleNum, chNum)

    hTOA = ROOT.TH1F("hist_toa_" + cycleID, "hist_toa_" + cycleID, 100, 0, 50)
    hAmp = ROOT.TH1F("hist_peak_" + cycleID, "hist_peak_" + cycleID, 100, 0, 50)
    for frame, subsample in enumerate(tfrac):
        # print(" 1 frame [%.2f], subsample [%.2f]"%(frame, subsample))
        # print(voltsAbs[:,frame],times[:,frame])
        # exit(0)

        pulsetime = times[:, frame]
        pulsevolt = voltsAbs[:, frame]
        if len(pulsetime) == 0 or len(pulsevolt) == 0:
            continue

        maxindex = EvalMaximum(pulsevolt)[1]
        if maxindex < (len(pulsevolt) / 2):

            pedestal = np.mean(pulsevolt[-nSamplesForNoise:])
            if chNum == 5:
                pedestal = -0.1  # 0.
            pulsevolt = np.subtract(pulsevolt, pedestal)
            noise = np.std(pulsevolt[-nSamplesForNoise:])
            pulseHeightnoise = EvalMaximum(pulsevolt[-nSamplesForNoise:])[0]
        else:
            pedestal = np.mean(pulsevolt[:nSamplesForNoise])
            if chNum == 5:
                pedestal = -0.1
            pulsevolt = np.subtract(pulsevolt, pedestal)
            noise = np.std(pulsevolt[:nSamplesForNoise])
            pulseHeightnoise = EvalMaximum(pulsevolt[:nSamplesForNoise])[0]

        branches["pedestal"].append(pedestal)
        branches["noise"].append(noise)
        branches["pulseHeightnoise"].append(pulseHeightnoise)

        timestamp = tdate[Nwfm] + tdatefrac[Nwfm]
        branches["timestamp"].append(timestamp)

        maxAndTime = EvalMaximum(pulsevolt)
        Amax = maxAndTime[0]
        maxindex = maxAndTime[1]
        timeAtMax = pulsetime[maxindex]
        branches["Amax"].append(Amax)
        branches["timeAtMax"].append(timeAtMax)

        ##Now Amax is evaluated after pedestal subtruction
        # Amax=EvalMaximum(voltsAbs[:,frame])
        # branches['Amax'].append(Amax)

        hAmp.Fill(Amax * 1000.0)  # unit mV

        charge_min = maxindex - int(2e-9 / tscale)
        charge_max = maxindex + int(2e-9 / tscale)
        charge = tscale * sum(pulsevolt[charge_min:charge_max]) / Transimpedance * 1e12

        branches["charge"].append(charge)

        ped = 0
        fCFD = 0.5
        # th = fCFD * (Amax - ped)
        th = fCFD * Amax

        tCFD = 0
        # tCFD=EvalCFD(voltsAbs[:,frame],times[:,frame],th, i = 0)
        # branches['tCFD'].append(float(tCFD))

        tCFD = EvalCFD(
            pulsevolt[0 : maxindex + 1], pulsetime[0 : maxindex + 1], th, i=0
        )
        vthc = EvalCFD(
            pulsevolt[0 : maxindex + 1], pulsetime[0 : maxindex + 1], 10 / 1000.00, i=0
        )
        # if(Amax>200)
        branches["tCFD"].append(float(tCFD))

        tCFD20 = EvalCFD(
            pulsevolt[0 : maxindex + 1], pulsetime[0 : maxindex + 1], 0.2 * Amax, i=0
        )

        branches["tCFD20"].append(float(tCFD20))

        tCFD70 = EvalCFD(
            pulsevolt[0 : maxindex + 1], pulsetime[0 : maxindex + 1], 0.7 * Amax, i=0
        )

        branches["tCFD70"].append(float(tCFD70))

        tCFD10 = EvalCFD(
            pulsevolt[0 : maxindex + 1], pulsetime[0 : maxindex + 1], 0.1 * Amax, i=0
        )

        tCFD90 = EvalCFD(
            pulsevolt[0 : maxindex + 1], pulsetime[0 : maxindex + 1], 0.9 * Amax, i=0
        )

        risetime = tCFD90 - tCFD10
        if risetime < 0:
            risetime = 0
        branches["risetime"].append(float(risetime))

        thre = 10 * 1e-3

        tCTD = EvalCFD(
            pulsevolt[0 : maxindex + 1], pulsetime[0 : maxindex + 1], thre, i=0
        )
        TOE = EvalCFD(
            pulsevolt[maxindex:], pulsetime[maxindex:], thre, i=0, inverse=True
        )
        # TOT = TOE - tCTD
        tCTD = vthc
        TOT = TOE - vthc
        alvinTOT = 0.1  # getalvinTOT()
        # CLK=EvalCFD3(pulsevolt,pulsetime,thre, i = 0)
        c1 = EvalCFD3(pulsevolt, pulsetime, thre, i=0)
        # if 0:#c1[0]==-99 and chNum == 6:
        #   plt.plot(pulsetime,pulsevolt)
        #   plt.savefig("fig/999.png")
        # for i in range(0,len(pulsevolt)):
        #     print("i",i,"a",pulsevolt[i])
        # exit(0)
        # print( "99!!!!!!!!")
        #     time.sleep(1000)
        # CLK=EvalCFD3(pulsevolt,pulsetime,thre, i = 0)
        branches["tCTD"].append(float(tCTD))
        branches["TOT"].append(float(TOT))
        branches["CLK"].append(float(c1[0]))
        branches["CLK2"].append(float(c1[0]))
        branches["CLK3"].append(float(c1[0]))
        branches["CLK4"].append(float(c1[0]))
        Nwfm = Nwfm + 1

    tAnaEnd = time.time()
    print(" [*] Analyze done... takes [%.3fs]" % (tAnaEnd - tPlottingEnd))

    if doPlot:
        plt.savefig(
            "plots/plt_r{:05d}_c{:04d}_ch{:d}.png".format(runNum, cycleNum, chNum),
            bbox_inches="tight",
        )
        c = ROOT.TCanvas("c_" + cycleID, "c_" + cycleID, 600, 400)
        hAmp.Draw("hist")
        c.SaveAs("plots/Amax_{}.pdf".format(cycleID))
    tSaveEnd = time.time()
    print(" [*] Fig Saving done... takes [%.3fs]" % (tSaveEnd - tAnaEnd))

    # plt.show()


def EvalMaximum(pulse):
    if len(pulse) == 0:
        return -999.0
    index = np.argmax(pulse)
    maxValue = pulse[index]
    return (maxValue, index)


# def EvalCFD(a,b,t=0.1, i = 0):
#        if len(a) == 0:
#            print("warning, waveform length = 0!!!")
#            return 0
#        if a[0] > t:
#             return i
#        else:
#            aoverth = np.nonzero(a > t)[0]
#            if len(aoverth) == 0:
#                #print("warning, no over threshold one....")
#                return 0
#            else:
#                index2 = np.nonzero(a > t)[0][0]
#                if index2 ==  0:
#                    print("warning, tCFD at beginning  bad waveform....")
#                    return 0
#                index1 = index2 - 1
#                slope = float(a[index2] - a[index1])/(b[index2] - b[index1])
#
#                tCFD = b[index1] + (t - a[index1])/slope
#
#                return tCFD   #at 6.25GS


def EvalCFD3(a, b, t=0.1, i=0, inverse=False):
    tt = 0
    tt2 = []
    for i in range(0, len(a) - 1):
        if a[i] > -150 / 1000.0 and a[i + 1] <= -150 / 1000.0:
            tt = b[i] * 1e12  # in ps
            slope = float(a[i + 1] - a[i]) / (b[i + 1] - b[i])
            tt = b[i] + (-150 / 1000.0 - a[i]) / slope
            # tt2.append(tt)#
            # print(len(a)," ",i," ",a[i]," ",b[i]*1e12,"!!!")
            break
    tt2.append(tt * 1e12)

    return tt2


def EvalCFD(a, b, t=0.1, i=0, inverse=False):
    flag = 0
    # print(len(a)," ",a[0]*1000," ",a[-1]*1000)
    # if(a[0]>200):
    # flag=1
    # print(a[1]*1000," ",a[-1]*1000)
    if len(a) == 0:
        # print("warning, waveform length = 0!!!")
        return 0
    if a[0] > t and not inverse:
        return i
    if a[0] < t and inverse:
        return i
    else:
        indexlist = np.nonzero(a < t)
        aoverth = indexlist[0]
        if len(aoverth) == 0:
            # print("warning, no over threshold one....")
            return 0
        else:
            if inverse:
                index2 = indexlist[0][0]
                index1 = index2 - 1
            else:
                index2 = indexlist[0][-1]
                index1 = index2 + 1

            if index2 == 0 or index2 == (len(a) - 1):
                # print("warning, tCFD at beginning  bad waveform....")
                return 0

            slope = float(a[index2] - a[index1]) / (b[index2] - b[index1])
            tCFD = b[index1] + (t - a[index1]) / slope
            return tCFD  # at 6.25GS


def main():
    global dataRoot
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-d", "--directory", help="", default=None)
    parser.add_option("-o", "--output", help="", default=None)
    parser.add_option("-n", "--channels", help="", default=5)
    (options, args) = parser.parse_args()

    print("checking input dir.....")
    print(f" --- {options.directory}")

    if not os.path.isdir(options.directory):
        print("error: invalidate path, returning...")
        return

    # print("checking output dir.....")
    # # if not os.path.isdir(options.output):
    # #     print("error: invalidate output path, returning...")
    # #     return

    runNumber = int(options.directory.split("/")[-1].replace("run", ""))
    dataRoot = options.directory.split("/run")[0]
    print(dataRoot)

    files_list = list_files_in_repository(options.directory)
    channels = get_channels(files_list)
    nCh = len(channels)

    print(f"\n --- These channels are present in {runNumber}: {channels} --- ")

    # outfilename = options.output + '/' + 'tree_{:05d}.root'.format(runNumber)
    outfilename = options.output

    if os.path.isfile(outfilename):
        print("File: ", outfilename, " exists continue...")

        return

    outfile = ROOT.TFile.Open(outfilename, "RECREATE")

    tree = ROOT.TTree("tree", "tree")

    branchs = {}

    branchs["eventnumber"] = array("i", [0])
    tree.Branch("eventnumber", branchs["eventnumber"], "eventnumber/I")

    branchs["timestamp"] = array("d", [0.0])
    tree.Branch("timestamp", branchs["timestamp"], "timestamp/D")

    # Nch = nCh
    Nch = 8

    branchs["Amax"] = array("f", Nch * [0.0])
    tree.Branch("Amax", branchs["Amax"], "Amax[" + str(Nch) + "]/F")

    branchs["tCFD"] = array("f", Nch * [0.0])
    tree.Branch("tCFD", branchs["tCFD"], "tCFD[" + str(Nch) + "]/F")

    branchs["tCTD"] = array("f", Nch * [0.0])
    tree.Branch("tCTD", branchs["tCTD"], "tCTD[" + str(Nch) + "]/F")

    branchs["TOT"] = array("f", Nch * [0.0])
    tree.Branch("TOT", branchs["TOT"], "TOT[" + str(Nch) + "]/F")

    branchs["tCFD20"] = array("f", Nch * [0.0])
    tree.Branch("tCFD20", branchs["tCFD20"], "tCFD20[" + str(Nch) + "]/F")

    branchs["tCFD70"] = array("f", Nch * [0.0])
    tree.Branch("tCFD70", branchs["tCFD70"], "tCFD70[" + str(Nch) + "]/F")

    branchs["risetime"] = array("f", Nch * [0.0])
    tree.Branch("risetime", branchs["risetime"], "risetime[" + str(Nch) + "]/F")

    branchs["noise"] = array("f", Nch * [0.0])
    tree.Branch("noise", branchs["noise"], "noise[" + str(Nch) + "]/F")

    branchs["pulseHeightnoise"] = array("f", Nch * [0.0])
    tree.Branch(
        "pulseHeightnoise",
        branchs["pulseHeightnoise"],
        "pulseHeightnoise[" + str(Nch) + "]/F",
    )

    branchs["pedestal"] = array("f", Nch * [0.0])
    tree.Branch("pedestal", branchs["pedestal"], "pedestal[" + str(Nch) + "]/F")

    branchs["charge"] = array("f", Nch * [0.0])
    tree.Branch("charge", branchs["charge"], "charge[" + str(Nch) + "]/F")

    branchs["timeAtMax"] = array("f", Nch * [0.0])
    tree.Branch("timeAtMax", branchs["timeAtMax"], "timeAtMax[" + str(Nch) + "]/F")

    branchs["CLK"] = array("f", Nch * [0.0])
    tree.Branch("CLK", branchs["CLK"], "CLK[" + str(Nch) + "]/F")
    branchs["CLK2"] = array("f", Nch * [0.0])
    tree.Branch("CLK2", branchs["CLK2"], "CLK2[" + str(Nch) + "]/F")

    branchs["CLK3"] = array("f", Nch * [0.0])
    tree.Branch("CLK3", branchs["CLK3"], "CLK3[" + str(Nch) + "]/F")
    branchs["CLK4"] = array("f", Nch * [0.0])
    tree.Branch("CLK4", branchs["CLK4"], "CLK4[" + str(Nch) + "]/F")

    eventNumber = 0
    for tekFiles in natural_sort(os.listdir(options.directory)):
        print(" file:", tekFiles)
        # continue

        if ".wfm" not in tekFiles or "ref" in tekFiles:
            continue

        if "ch{:d}".format(nCh) not in tekFiles:
            continue

        icycle = int(tekFiles.split("_")[0][3:])
        # if icycle > 5:
        #     break

        # ich = int(tekFiles.split('_')[1][2])
        chBanches = {}
        print(channels)
        for ich in channels:
            # for ich in range(1,nCh+1):
            chBanches[ich] = {}
            chBanches[ich]["eventnumber"] = []
            chBanches[ich]["timestamp"] = []
            chBanches[ich]["Amax"] = []
            chBanches[ich]["tCFD"] = []
            chBanches[ich]["tCTD"] = []
            chBanches[ich]["TOT"] = []
            chBanches[ich]["tCFD20"] = []
            chBanches[ich]["tCFD70"] = []
            chBanches[ich]["risetime"] = []
            chBanches[ich]["noise"] = []
            chBanches[ich]["pulseHeightnoise"] = []
            chBanches[ich]["pedestal"] = []
            chBanches[ich]["charge"] = []
            chBanches[ich]["timeAtMax"] = []
            chBanches[ich]["CLK"] = []
            chBanches[ich]["CLK2"] = []
            chBanches[ich]["CLK3"] = []
            chBanches[ich]["CLK4"] = []
            Converter(runNumber, icycle, ich, chBanches[ich])
            # print('get ch',ich,chBanches[ich])
        # for ich in range(2,nCh+1):
        for ich in channels:
            if len(chBanches[ich]["Amax"]) != len(chBanches[1]["Amax"]):
                print(
                    "warning, channel inconsistency found....!!! ch",
                    ich,
                    " nwf:",
                    len(chBanches[ich]["Amax"]),
                    " while ch 1, nfw:",
                    len(chBanches[1]["Amax"]),
                )
        for ievent in range(0, len(chBanches[1]["Amax"])):
            # for ich in range(1,nCh+1):
            for ich in channels:
                branchs["Amax"][ich - 1] = (
                    chBanches[ich]["Amax"][ievent] * 1.0e3
                )  # Unit : mV
                branchs["tCFD"][ich - 1] = (
                    chBanches[ich]["tCFD"][ievent] * 1.0e12
                )  # Unit : ps
                branchs["noise"][ich - 1] = (
                    chBanches[ich]["noise"][ievent] * 1.0e3
                )  # Unit : mV
                branchs["pulseHeightnoise"][ich - 1] = (
                    chBanches[ich]["pulseHeightnoise"][ievent] * 1.0e3
                )  # Unit : mV
                branchs["pedestal"][ich - 1] = (
                    chBanches[ich]["pedestal"][ievent] * 1.0e3
                )  # Unit : mV
                branchs["tCTD"][ich - 1] = (
                    chBanches[ich]["tCTD"][ievent] * 1.0e12
                )  # Unit : ps
                branchs["TOT"][ich - 1] = (
                    chBanches[ich]["TOT"][ievent] * 1.0e12
                )  # Unit : ps
                branchs["tCFD20"][ich - 1] = (
                    chBanches[ich]["tCFD20"][ievent] * 1.0e12
                )  # Unit : ps
                branchs["tCFD70"][ich - 1] = (
                    chBanches[ich]["tCFD70"][ievent] * 1.0e12
                )  # Unit : ps
                branchs["risetime"][ich - 1] = (
                    chBanches[ich]["risetime"][ievent] * 1.0e12
                )  # Unit : ps
                branchs["charge"][ich - 1] = chBanches[ich]["charge"][
                    ievent
                ]  # Unit : fC
                branchs["timeAtMax"][ich - 1] = (
                    chBanches[ich]["timeAtMax"][ievent] * 1.0e12
                )  # Unit : ps
                branchs["CLK"][ich - 1] = chBanches[ich]["CLK"][
                    ievent
                ]  # *1.0e12 #Unit : ps
                branchs["CLK2"][ich - 1] = chBanches[ich]["CLK2"][
                    ievent
                ]  # *1.0e12 #Unit : ps
                branchs["CLK3"][ich - 1] = chBanches[ich]["CLK3"][
                    ievent
                ]  # *1.0e12 #Unit : ps
                branchs["CLK4"][ich - 1] = chBanches[ich]["CLK4"][
                    ievent
                ]  # *1.0e12 #Unit : ps
            branchs["eventnumber"][0] = int(eventNumber)
            # print("ts",chBanches[1] )
            # print("ts",chBanches[1]['timestamp'][ievent] )

            branchs["timestamp"][0] = chBanches[1]["timestamp"][ievent]  # Unit : s
            eventNumber = eventNumber + 1
            tree.Fill()
        print(" file:", tekFiles, icycle, ich)

    tree.Write()
    outfile.Write()
    outfile.Close()


if __name__ == "__main__":

    main()

# for runNumber in [546]:
#      for icycle in [0,5]:
#            for ich in [1,2,3,4,5,6,7,8]:
#                Converter(runNumber,icycle,ich)
