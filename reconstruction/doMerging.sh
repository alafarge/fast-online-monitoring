#!/bin/bash
base=/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam

# arguments
campaign=${1}
run=${2}
clock_ch=${3} 
probe_ch=${4} 
mcp_ch=${5}

file1="${base}/${campaign}/Alvin/ntuples/Alvin-${campaign}-${run}.root"
file2="${base}/${campaign}/Digitizer/ntuples/Digitizer-${campaign}-${run}.root"

# merge_file="ntuples/Merged-${campaign}-${run}.root"
merge_file="${base}/${campaign}/Merged/Merged-${campaign}-${run}.root"


# Function to check for file existence
check_files_exist() {
    campaign=${1}
    run=${2}

    # Construct file names
    echo "----- Checking for files..."
    file1="${base}/${campaign}/Alvin/ntuples/Alvin-${campaign}-${run}.root"
    file2="${base}/${campaign}/Digitizer/ntuples/Digitizer-${campaign}-${run}.root"

    # Check if both files exist
    if [[ -f "$file1" && -f "$file2" ]]; then
        echo "----- Both files exist."
        return 1 # success 
    else
        echo "----- One or both files do not exist."
        return 0 # error
    fi
}

check_files_exist ${campaign} ${run}
ret=$?

timeout=3600
start_time=$SECONDS
reco=1

if [[ $ret -eq 1 ]]; then
    echo "----- Waiting for files to finish writing..."
    # while lsof -w "$file1" >/dev/null || lsof -w "$file2" >/dev/null; do
    #     if (( SECONDS - start_time >= timeout )); then
    #         reco=0
    #         echo "The timeout of $timeout seconds has been reached."
    #         break
    #     fi
    #     sleep 20
    # done
    if [[ $reco -eq 1 ]]; then
        echo "----- Both files have finished writing."
        echo "----- Executing merging between Alvin and Digitizer ..."
        err_file="reconstruction/log/jobs/job-Merged-${campaign}-${run}.err"
        # Execute Python script with the file names as arguments
	echo "----- Creating $merge_file"
        python3 "reconstruction/Matching.py" "$file1" "$file2" "$merge_file" "$clock_ch" "$probe_ch" "$mcp_ch" > "$err_file" 2>&1

    fi
else
    echo "--------->  Error: One or both files do not exist."
fi
sleep 2