import ROOT 
import argparse

parser = argparse.ArgumentParser(description='Convert Alvin data to root file')
parser.add_argument('input', metavar='input', type=str, help='input file name .dat')
parser.add_argument('output', metavar='output', type=str, help='output file name .root')


if __name__ == "__main__":
    args = parser.parse_args()
    inFileName = args.input
    outFileName = args.output

    ROOT.gROOT.LoadMacro('reconstruction/asic/alvin2root.C')
    # ROOT.gInterpreter.ProcessLine('#include alvin2root.C')

    file_name = ROOT.std.string(inFileName)
    output_name = ROOT.std.string(outFileName)
    ROOT.alvin2root( file_name, output_name)