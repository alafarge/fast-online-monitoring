import ROOT
import numpy as np
from array import array
import os, sys
import argparse
import time
ROOT.gROOT.SetBatch(True)
sys.path.append('./utils')
import settings as s

# get the arguments
parser = argparse.ArgumentParser(description='Merge Digitizer and ASIC data')
parser.add_argument('asic_file', metavar='asic_file', type=str, help='asic file name .root')
parser.add_argument('digitizer_file', metavar='digitizer_file', type=str, help='probe file name .root')
parser.add_argument('save_file', metavar='save_file', type=str, help='save file name .root')
parser.add_argument('clock_ch', metavar='clock_ch', type=int, help='Channel of the clock signal')
parser.add_argument('probe_ch', metavar='probe_ch', type=int, help='Channel of the probe signal')
parser.add_argument('mcp_ch', metavar='mcp_ch', type=int, help='Channel of the MCP signal')
# parser.add_argument('probe_pix', metavar='probe_pixel', type=list, help='Give under this list format [col,row] of probe_pixel')

def to_pixel(col, row):
    pixels = []
    for i in range(len(col)):
        pixels.append(col[i]*15+row[i])
    
    return pixels

def merging(asicFileName, probeFileName, save_file, chClock, chProbe, chMCP,  toa_lsb_file=None, tot_lsb_file=None):
    #Get the values from TOA/TOT LSB scan
    if toa_lsb_file is not None:
        with open(f"{toa_lsb_file}", "r") as file:
            values_toaLSB = [float(line.strip()) for line in file]
    else: 
        values_toaLSB = [18]*225
        

    if tot_lsb_file is not None:
        with open(f"{tot_lsb_file}", "r") as file:
            values_totLSB = [float(line.strip()) for line in file]
    else:
        values_totLSB=[1]*225
            
    ## TO BE CHANGE ##
    # values_toaLSB=20
    # values_totLSB=1
    ##################
    
    asicfile = ROOT.TFile.Open(asicFileName,"READ")
    asictree = asicfile.Get("tree") 

    probefile =  ROOT.TFile.Open(probeFileName,"READ")
    probetree = probefile.Get("tree") 

    # asictree.Print()
    # probetree.Print()

    hIntervalDifference = ROOT.TH1F("time interval difference between fpga ts and digitizer ts","time interval difference between fpga ts and digitizer ts",200,-1e-6,1e-6)
    hIntervalDifferenceCrose =  ROOT.TH1F("time interval difference between fpga ts and digitizer ts","time interval difference between fpga ts and digitizer ts",200,-1e-3,1e-3)
    hIntervalDifferenceCrose2D =  ROOT.TH2F("time interval difference between fpga ts and digitizer ts","time interval difference between fpga ts and digitizer ts;x;y",500,0e3,500e3,500,-1e-3,1e-3)
    hTSDifference2D =  ROOT.TH2F("time stamp difference between fpga ts and digitizer","time stamp difference between fpga ts and digitizer;event;time stamp difference between fpga ts and digitizer",500,0e3,500e3,500,-1e-1,1e-1)
    hTOTCorr = ROOT.TH2F("tot probe vs tot altiroc", "tot probe vs tot altiroc; tot probe ; tot altiroc",100,1.5e3,5e3,100,1e3,12e3)
    hTOTAmaxCorr = ROOT.TH2F("probe amplitude vs tot altiroc", " probe amplitude vs tot altiroc; tot_altiroc [ps] ; probe amplitude [mV]",50,1.5e3,5e3,50,10,45)
    hTOAAmaxCorr = ROOT.TH2F("-toa_altiroc*LSB_scan - (MCP - TDC_CLK) vs probe amplitude (pixel: c5r4)", " tot_altiroc*LSB_scan vs toa_altiroc*LSB_scan - (MCP - TDC_CLK);-toa_altiroc*LSB_scan - (MCP - TDC_CLK);probe amplitude",100,10,45,100,-9.8e3,-9.1e3)
    hTOA1D = ROOT.TH1F("toa_altiroc*LSB_scan - (MCP - TDC_CLK)", "toa_altiroc*LSB - (MCP - TDC_CLK); toa_altiroc*LSB_scan - (MCP - TDC_CLK)",100,-10.5e3,-9e3)
    hTOAAmaxCorrDebug = ROOT.TH2F("toa_altiroc vs MCP - TDC CLK", "toa_altiroc vs MCP - TDC CLK ; toa_altiroc; (MCP - TDC_CLK)",50,0,2.5e3,50,-10e3,-6.5e3)
    hTOATOTCorr = ROOT.TH2F("tot_altiroc vs toa_altiroc - (MCP - TDC_CLK)", "tot_altiroc vs toa_altiroc - (MCP - TDC_CLK); tot_altiroc ;toa_altiroc - (MCP - TDC_CLK)",40,2e3,4e3,100,-10e3,-8.8e3)

    print("file asic have entries ",asictree.GetEntries())
    print("file probe have entries ",probetree.GetEntries())

    asicT0=-1.0
    paT0=-1.0

    asicT=-1.0
    paT=-1.0

    asicTBefore = -1.0
    paTBefore = -1.0

    asicCycle = 0
    asicClockPeriod = 2**31

    asicEventShift = 0
    probeEventShift = 0

    asicClockFreq = 160.0e6

    satTOACounter = 0

    nLarge=0
    nSmall=0
    nEqual=0

    #asic clock cycle=2**31 / 160e6

    counterPixel54=0
    counterPixel54TOANonSat=0
    counterSync=0

    #--- preparing output file and structure
    print(save_file)
    ofilename = save_file

    fo = ROOT.TFile.Open(ofilename,"recreate")
    to = ROOT.TTree("Sequoia", "Sequoia")

    NHits   = array('i',[ 0 ])
    tTOA    = array('d',[ 0 ])
    tTOT    = array('d',[ 0 ])
    coarse  = array('d',[ 0 ])
    fine    = array('d',[ 0 ])
    tMCP    = array('d',[ 0 ])
    aMCP    = array('d',[ 0 ])
    tClock  = array('d',[ 0 ])
    tProbe  = array('d',[ 0 ])
    aProbe  = array('d',[ 0 ])
    nCol    = array('i',[ 0 ])
    nRow    = array('i',[ 0 ])

    to.Branch('NHits',NHits,'NHits/I')
    to.Branch('tTOA',tTOA,'tTOA/D')
    to.Branch('tTOT',tTOT,'tTOT/D')
    to.Branch('coarse',coarse,'coarse/D')
    to.Branch('fine',fine,'fine/D')
    to.Branch('tMCP',tMCP,'tMCP/D')
    to.Branch('aMCP',aMCP,'aMCP/D')
    to.Branch('tClock',tClock,'tClock/D')
    to.Branch('aProbe',aProbe,'aProbe/D')
    to.Branch('tProbe',tProbe,'tProbe/D')
    to.Branch('nCol',nCol,'nCol/I')
    to.Branch('nRow',nRow,'nRow/I')

    c_filled=0
    multipleHits=False
    
    numTrig0=-1
    numEvent0=-1
    for i in range(0,asictree.GetEntries()):
        if (i+asicEventShift) == asictree.GetEntries() or i==probetree.GetEntries():
            break

        if i > 1000000: 
            continue
        
        asictree.GetEntry(i + asicEventShift)
        if asictree.nhits==0:
            continue
        
        if asictree.nhits==1:
            pos = 0
        
        if asictree.nhits>1:
            pos=np.argmax(list(asictree.tot))
        
        probetree.GetEntry(i)
        
        numTrig = asictree.triggerNumber
        numEvent0 = probetree.eventnumber 
    
        #Apply the values from TOA/TOT LSB scans
        toaLSB = asictree.toa[pos] * values_toaLSB[asictree.row[pos] + asictree.col[pos]*15]

        print("matched", "triggerNumber",asictree.triggerNumber,"probe eventnumber",probetree.eventnumber)
        if asicT0 < 0.0:
            asicT0 = asictree.m_timestamp/asicClockFreq
            paT0 = probetree.timestamp

        asicT = asictree.m_timestamp/asicClockFreq - asicT0  +  asicCycle * asicClockPeriod / asicClockFreq
        paT = probetree.timestamp - paT0

        if asicT - asicTBefore < 0 and asicTBefore > 0:
            asicCycle = asicCycle + 1
        asicT = asictree.m_timestamp/asicClockFreq - asicT0  +  asicCycle * asicClockPeriod / asicClockFreq

        # print(asicT, asicTBefore)
        # print(paT, paTBefore)
        # time.sleep(1)
        
        # while abs(asicT - paT)  > 0.04:
        # while 0:
        #     # print("shifting triggerNumber",asictree.triggerNumber,"probe eventnumber",probetree.eventnumber)
        #     print("time desync:  asicT:",asicT,"probeT",paT, "dT",abs(asicT - paT), "shift",probeEventShift,asicEventShift)
        #     if asicT > paT:
        #         probeEventShift = probeEventShift + 1
        #         probetree.GetEntry(i + probeEventShift)
        #         paTBefore = paT          
        #     else:
        #         asicEventShift = asicEventShift + 1
        #         asictree.GetEntry(i + asicEventShift)
        #         asicTBefore =  asicT     
        #         # asicT = asictree.m_timestamp/asicClockFreq - asicT0  +  asicCycle * asicClockPeriod / asicClockFreq

        #     asicT = asictree.m_timestamp/asicClockFreq - asicT0  +  asicCycle * asicClockPeriod / asicClockFreq
        #     paT = probetree.timestamp - paT0

        #     if asicT - asicTBefore < 0 and asicTBefore > 0:
        #         asicCycle = asicCycle + 1
        #     asicT = asictree.m_timestamp/asicClockFreq - asicT0  +  asicCycle * asicClockPeriod / asicClockFreq
        #     print(i,"syncing time: new asicT:",asicT,"probeT",paT)
        #     # if asicEventShift > 50000 or probeEventShift > 5000:
        #     if asicEventShift > 500000 or probeEventShift > 50000:
        #         break
                # exit()

        hTSDifference2D.Fill(i,asicT-paT)

        if i % 20000 == 0:
            print("processing events",i," .....")
        
        if (asicT-asicTBefore)-(paT-paTBefore)  > 500e-9 or (asicT-asicTBefore)-(paT-paTBefore)  < -500e-9:
            print("warning, time out of sync... timestamp difference larger than 500ns dTs:",(asicT-asicTBefore)-(paT-paTBefore),  asicT-paT)
            # print((asicT-asicTBefore)-(paT-paTBefore),  asicT-paT)
            # exit()

        hIntervalDifference.Fill( (asicT-asicTBefore)-(paT-paTBefore) )
        hIntervalDifferenceCrose.Fill( (asicT-asicTBefore)-(paT-paTBefore) )
        hIntervalDifferenceCrose2D.Fill(i,(asicT-asicTBefore)-(paT-paTBefore) )

        tdc_clock = probetree.CLK[chClock-1]
        halfPeriod = probetree.CLK[chClock-1] - probetree.CLK3[chClock-1]

        deltaT = -toaLSB - (probetree.tCFD[chMCP-1] - tdc_clock)
        if True:
            
            if (tdc_clock < s.tdc_clock_cut and asictree.toa[pos] > 0 and asictree.toa[pos] < 127 and probetree.Amax[chMCP-1] > s.probe_amblitude_cut) and (asictree.nhits == 1 or asictree.tot[pos]>2000) : #and probetree.Amax[1-1]<140

            # if True:

                # hTOA1D.Fill(deltaT)
                # hTOATOTCorr.Fill(totLSB,deltaT) 
                # hTOAAmaxCorr.Fill(probetree.Amax[6],deltaT) 
                # hTOTCorr.Fill(totLSB,probetree.TOT[6])
                
                    
                # if asictree.col == 7 and asictree.row == 5 and probetree.Amax[4-1] > 10:
                #     hTOAAmaxCorr.Fill(probetree.Amax[chProbe-1],deltaT) 
                #     hTOTAmaxCorr.Fill(totLSB,probetree.Amax[6])
                #     hTOAAmaxCorrDebug.Fill(toaLSB, -probetree.tCFD[1-1] + tdc_clock)
                #     hTOTCorr.Fill(totLSB,probetree.TOT[4-1])
                #     hTOATOTCorr.Fill(totLSB,deltaT) 
                #     hTOA1D.Fill(deltaT)
                
                tTOA[0]=-asictree.toa[pos]
                tTOT[0]=asictree.tot[pos]
                coarse[0]=asictree.coarse[pos]
                fine[0]=asictree.fine[pos]
                NHits[0]=asictree.nhits
                tMCP[0]=probetree.tCFD[chMCP-1]
                aMCP[0]=probetree.Amax[chMCP-1]
                aProbe[0]=probetree.Amax[chProbe-1]
                tProbe[0]=probetree.tCFD[chProbe-1]
                nCol[0]=asictree.col[pos]
                nRow[0]=asictree.row[pos]
                tClock[0]=tdc_clock

                to.Fill()
        

        else:
            print("reject ", tdc_clock, probetree.Amax[chProbe-1], toaLSB, toaLSB)

        asicTBefore = asicT
        paTBefore = paT


    fo.Write()
    fo.Close()
    
    print(f'File {ofilename} has been filled')

if __name__ == "__main__":
    toa_lsb_file="/afs/cern.ch/user/h/hgtdtb/Software/Scripts/Cron/example/LSB_scans/toa_1.txt"
    tot_lsb_file="/afs/cern.ch/user/h/hgtdtb/Software/Scripts/Cron/example/LSB_scans/tot_1.txt"
    
    args = parser.parse_args()
    asic_f=args.asic_file
    digitizer_file=args.digitizer_file
    save_file=args.save_file
    clock_channel = args.clock_ch
    probe_channel=args.probe_ch
    mcp_channel=args.mcp_ch
    
    merging(asic_f, digitizer_file, save_file,clock_channel, probe_channel, mcp_channel, toa_lsb_file=None, tot_lsb_file=None)