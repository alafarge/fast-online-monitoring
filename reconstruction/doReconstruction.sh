#!/bin/bash -f
# expect 2 arguments: campaign and run number
# data stored in ${base}/campaign/{Digitizer,Alvin}/runnumber
# usage: source process-run.sh campaign runnumber

# arguments
run=${2}
campaign=${1}
force=${3} # if 1, ignore checks related to previous processings and do the processing

# parameters
base=/eos/atlas/atlascerngroupdisk/det-hgtd/testbeam
whenStopProcessing=604800 # = 1 week. Runs older than this delay will be ignored

echo "-------------------------------------------------------"

# Function to process a repository (Digitizer or Alvin)
process_repository() {
    local repository=${1}
    local base=${2}
    local campaign=${3}
    local run=${4}
    local force=${5}
    local whenStopProcessing=${6}

    # if [[ ${run} -lt 10000 ]];
	# then
	#    srun=${run:1} #no 0
	# else
	#    srun=${run}
    # fi


    if [[ ${repository} == "Digitizer" ]]
        then
        if [[ -d ${base}/${campaign}/${repository}/run${run}/ ]]
        then
            echo "----- ${repository} directory found."
        else
            echo "----- ${repository} directory: ${base}/${campaign}/${repository}/run${run}/ not found"
        fi

    elif [[ ${repository} == "Alvin" ]];then
        if [[ -f "${base}/${campaign}/${repository}/EUDAQ_SCAN_${srun}.dat" ]]
        then
            echo "----- ${repository} .dat file found."
        else
            echo "----- ${repository} .dat: ${base}/${campaign}/${repository}/EUDAQ_SCAN_${srun}.dat not found"
        fi
    fi

    toBeReconstructed=0 # 1 will be reconstructed, 0 not

    # Conditions for reconstruction
    # if [ ${repository} == "Digitizer" ]; then
    #     lastdata=`stat -c %Y "${base}/${campaign}/${repository}/run${run}"/* | sort -n | tail -1`
    #     now=`date +%s`
    #     dataAge=$(( $now - $lastdata ))


    # elif [ ${repository} == "Alvin" ]; then
    #     lastdata=`stat -c %Y "${base}/${campaign}/${repository}/EUDAQ_SCAN_${srun}.dat"`
    #     now=`date +%s`
    #     dataAge=$(( $now - $lastdata ))
    # fi

    ## When was it processed
    # if [[ -f ntuples/${repository}-${campaign}-${run:1}.root ]];
    # then
    #     Processed=`date -r ntuples/${run}.root +%s`
    #     ntupleAge=$(( $Processed - $lastdata ))
    #     echo "----- Run already processed ("$(( ${ntupleAge} / 3600 ))" h) after data taking"
    # else
    #     ntupleAge=0 # not processed
    # fi

    # ##

    # if [ $dataAge -lt 600 ]; #last run was taken since less then 60mn 
    # then
    #     toBeReconstructed=1
    #     echo "----- Last run taken in the last 60mn ("${diff}") => to be reconstructed"
    # else
    #     echo "----- Run has more then 1h ("$(( ${dataAge} / 3600 )) "h)"
    #     if [ $dataAge -lt $whenStopProcessing ] && [ $ntupleAge -lt 600 ]; #last run was taken since less then 2 days
    #     then
    #     echo "     ... and less than "$(( $whenStopProcessing/3600 ))" h, ntuples are less then 1h older then data or do not exist => to be reconstructed"
    #     toBeReconstructed=1
    #     else
    #     echo "     ... but more than "$(( $whenStopProcessing/3600 ))" h OR ntuples are more recent then data "$(( ${ntupleAge}/3600 ))"h) => no reconstruction needed"
    #     fi
    # fi

    ##

    # if [[ ${force} -eq 1 ]]
    # then
    #     echo "----- Force reconstruction option is 1."
    #     toBeReconstructed=1
    # fi

    if [[ -f ntuples/processing-ongoing-run${run}.tmp ]]; #reconstruction is already ongoing (can take 1-2h for long runs)
    then
        echo "----- Digitizer reconstruction will not be launched, still ongoing."
        toBeReconstructed=0
    fi

    if [ ${repository} == "Digitizer" ]; then
        if [ -f "${base}/${campaign}/${repository}/ntuples/${repository}-${campaign}-${run}.root" ];then
            echo "----- File exists ----> ntuples/${repository}-${campaign}-${run}.root"
            toBeReconstructed=0
        else
            toBeReconstructed=1
        fi
    fi

    if [ ${repository} == "Alvin" ];then
        toBeReconstructed=1;
    fi
    ##
    
    # Running reconstruction 
    #####################################################################
    ####################### - D I G I T I Z E R - #######################
    #####################################################################
    if [[ $toBeReconstructed -gt 0 && ${repository} == "Digitizer" ]]; then

        nchannels=`ls -lrt ${base}/${campaign}/Digitizer/run${run}/Tek0000_ch* | wc -l`   
        echo "----- Number of digitizer channels "${nchannels}

        err_file_dig="./reconstruction/log/jobs/job-${repository}-${campaign}-${run}.err"
        touch "$err_file_dig"

        # Define the arguments for the Python script
        input_file_dig="${base}/${campaign}/${repository}/EUDAQ_SCAN_${srun}.dat"
        output_file_dig="${base}/${campaign}/Digitizer/ntuples/${repository}-${campaign}-${run}.root"

        python3 reconstruction/Digitizer.py -d "${base}/${campaign}/Digitizer/run${run}" -o "$output_file_dig" > "$err_file_dig" 2>&1
        
        #################################### Condor Jobs ####################################
        # echo "#!/bin/zsh
        # echo \"-- Running jobs/job-${repository}-${campaign}-${run}.sh\"
        # echo \"      on host `hostname` \"
        # cd /afs/cern.ch/user/h/hgtdtb/Software/Scripts/Cron/
        # touch ntuples/processing-ongoing-run${run}.tmp
        # rm ntuples/${repository}-${campaign}-${run}.root
        # ./reconstruction/Digitizer.py -d ${base}/${campaign}/Digitizer/run${run} -o ${output_file_dig}  > "$err_file_dig" 2>&1
        # rm -f ntuples/processing-ongoing-run${run}.tmp" > jobs/job-${repository}-${campaign}-${run}.sh
        # chmod u+x jobs/job-${repository}-${campaign}-${run}.sh
        # echo "
        # executable     = jobs/job-${repository}-${campaign}-${run}.sh
        # output         = jobs/job-${repository}-${campaign}-${run}.out
        # error          = jobs/job-${repository}-${campaign}-${run}.err
        # log            = jobs/job-${repository}-${campaign}-${run}.log
        # +MaxRunTime    = 9000
        # queue" > jobs/job-${repository}-${campaign}-${run}.sub

        # echo "-- Submitting job: jobs/job-${repository}-${campaign}-${run}.sh"
        # echo "                   condor_submit jobs/job-${repository}-${campaign}-${run}.sub"
        # condor_submit jobs/job-${repository}-${campaign}-${run}.sub
        ####################################################################################

    
    #####################################################################
    ####################### - A L V I N - ###############################
    #####################################################################
    elif [[ $toBeReconstructed -gt 0 && ${repository} == "Alvin" ]]; then
        err_file="./reconstruction/log/jobs/job-${repository}-${campaign}-${run}.err"
        # Define the arguments for the Python script
        input_file="${base}/${campaign}/${repository}/EUDAQ_SCAN_${srun}.dat"
        output_file="${base}/${campaign}/Alvin/ntuples/${repository}-${campaign}-${run}.root"

        python3 reconstruction/Alvin.py "$input_file" "$output_file" > "$err_file" 2>&1
    fi
}

# Process Alvin repository
echo ""
echo "-- ASIC data is converting to .root file"
echo "----- Check ./reconstruction/log/jobs/job-Alvin-${campaign}-${run}.err to debug"
process_repository "Alvin" "${base}" "${campaign}" "${run}" "${force}" "${whenStopProcessing}"
# cp "ntuples/Alvin-${campaign}-${run}.root" "${base}/${campaign}/Alvin/ntuples/"
echo ""
echo "######                                                                       ######"
echo "## Conversion is finished --> Alvin/ntuples/Alvin-${campaign}-${run}.root on EOS ##"
echo "######                                                                       ######"

echo "" 


echo "-- Digitizer output is converting to .root file"
echo "----- Check ./reconstruction/log/jobs/job-Digitizer-${campaign}-${run}.err to debug"

# Process Digitizer repository
timeout=3600
start_time=$SECONDS
reco=1
if [[ -f "jobs/job-${repository}-${campaign}-${run}.sh" ]];then
    echo "- Job still processing"
else
    process_repository "Digitizer" "${base}" "${campaign}" "${run}" "${force}" "${whenStopProcessing}"
fi

until [ -e "${base}/${campaign}/Digitizer/ntuples/Digitizer-${campaign}-${run}.root" ] || (( t++ >= 720 )); do
    sleep 10
done

# if [[ -f "ntuples/Digitizer-${campaign}-${run}.root" ]];then
#     cp "ntuples/Digitizer-${campaign}-${run}.root" "${base}/${campaign}/Digitizer/ntuples/"
# fi
if [ reco==1 ];then
    echo ""
    echo "######                                                                               ######"
    echo "## Conversion is finished --> Digitizer/ntuples/Digitizer-${campaign}-${run}.root on EOS ##"
    echo "######                                                                               ######"

else 
    echo "-- Condor was unable to process during the allocated time"
fi

echo ""
echo "----------------------------------------------------------------"
